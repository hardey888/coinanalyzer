package Presenter;

import com.example.hardeylim.coinanalyzer.MainActivity;
import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import apis.BinanceApi_v3;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import models.Binance.Ticker;
import models.Binance.TransactionLog;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hardeylim on 25/01/2018.
 */

public class NeoWatcherPresenter {

    private String v3_URL = "https://api.binance.com/api/v3/";
    private MainActivity activity;
    private DecimalFormat df = new DecimalFormat("##.##%");
    private TransactionLog transactionLog = new TransactionLog();

    List<String> neoPairs = new ArrayList<String>() {{
        add("NEOUSDT");
        add("NEOBTC");
        add("NEOBNB");
        add("NEOETH");
        add("BTCUSDT");
        add("BNBUSDT");
        add("ETHUSDT");
    }};

    List<String> ltcPairs = new ArrayList<String>() {{add("LTCUSDT");
        add("LTCBTC");
        add("LTCBNB");
        add("LTCETH");
        add("BTCUSDT");
        add("BNBUSDT");
        add("ETHUSDT");}};

    List<String> bccPairs = new ArrayList<String>() {{add("BCCUSDT");
        add("BCCBTC");
        add("BCCBNB");
        add("BCCETH");
        add("BTCUSDT");
        add("BNBUSDT");
        add("ETHUSDT");}};



    public NeoWatcherPresenter(MainActivity activity) {
        this.activity = activity;
    }

    private BinanceApi_v3 api_v3 = new Retrofit.Builder()
            .baseUrl(v3_URL)
            .client(new OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(BinanceApi_v3.class);


    public void watchNeoPairs() {
        NumberFormat defaultFormat = NumberFormat.getPercentInstance();

        Gson gson = new Gson();

        api_v3.getAllTickers().retry()
                                .delay(1, TimeUnit.SECONDS)
                                .repeat()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Consumer<Ticker[]>() {
                    @Override
                    public void accept(Ticker[] results) throws Exception {
                        List<Ticker> tickers = Arrays.asList(results);
                        List<Ticker> neoList = tickers.stream().filter(ticker -> neoPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                        List<Ticker> ltcList = tickers.stream().filter(ticker -> ltcPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                        List<Ticker> bccList = tickers.stream().filter(ticker -> bccPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                        processNeoTickers(tickers);
                        //System.out.println(gson.toJson(neoList));
                    }
                });
    }

    private void processNeoTickers(List<Ticker> tickers){
        Ticker NEOUSDTTicker = tickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOUSDT")).findFirst().get();

        Ticker NEOBTC = tickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOBTC")).findFirst().get();
        Ticker NEOBNB = tickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOBNB")).findFirst().get();
        Ticker NEOETH = tickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOETH")).findFirst().get();
        Ticker BTCUSDT = tickers.stream().filter(ticker -> ticker.getSymbol().equals("BTCUSDT")).findFirst().get();
        Ticker BNBUSDT = tickers.stream().filter(ticker -> ticker.getSymbol().equals("BNBUSDT")).findFirst().get();
        Ticker ETHUSDT = tickers.stream().filter(ticker -> ticker.getSymbol().equals("ETHUSDT")).findFirst().get();

        Double NEOUSD = NEOUSDTTicker.getPrice();
        Double NEOBTCUSDT = NEOBTC.getPrice() * BTCUSDT.getPrice();
        Double NEOETHUSDT = NEOETH.getPrice() * ETHUSDT.getPrice();
        Double NEOBNBUSDT = NEOBNB.getPrice() * BNBUSDT.getPrice();
        Ticker NEOBTCUSDTTicker = new Ticker("NEOBTCUSDT", NEOBTCUSDT);
        Ticker NEOETHUSDTTicker = new Ticker("NEOETHUSDT", NEOETHUSDT);
        Ticker NEOBNBUSDTTicker = new Ticker("NEOBNBUSDT", NEOBNBUSDT);

        List<Ticker> tickersInUSDT = new ArrayList<Ticker>();
        tickersInUSDT.add(NEOUSDTTicker);
        tickersInUSDT.add(NEOBTCUSDTTicker);
        tickersInUSDT.add(NEOETHUSDTTicker);
        tickersInUSDT.add(NEOBNBUSDTTicker);

        Ticker maxUSDT = tickersInUSDT.stream().max(Comparator.comparing(Ticker::getPrice)).get();
        Ticker minUSDT = tickersInUSDT.stream().min(Comparator.comparing(Ticker::getPrice)).get();
        Double netGain = maxUSDT.getPrice() - minUSDT.getPrice();
        Double percentageGain = netGain/minUSDT.getPrice();
        Date dateTime = new Date();
        dateTime.setTime(System.currentTimeMillis());

        if(netGain > transactionLog.getNetGain()){
            transactionLog.store(maxUSDT, minUSDT, netGain, percentageGain, System.currentTimeMillis(), dateTime);
        }

        Double NETNEOBTCUSDT = (NEOBTCUSDT - NEOUSD) / NEOUSD;
        Double NETNEOETHUSDT = (NEOETHUSDT - NEOUSD) / NEOUSD;
        Double NETNEOBNBUSDT = (NEOBNBUSDT - NEOUSD) / NEOUSD;

        System.out.println("NEO ->        USDT: " + String.format("%.10f", NEOUSDTTicker.getPrice()));
        System.out.println("NEO -> BTC -> USDT: " + String.format("%.10f", NEOBTC.getPrice() * BTCUSDT.getPrice()) + " NET: " + String.format("%.10f", NEOBTCUSDT - NEOUSD) + " Percentage: " + df.format(NETNEOBTCUSDT));
        System.out.println("NEO -> ETH -> USDT: " + String.format("%.10f", NEOETH.getPrice() * ETHUSDT.getPrice()) + " NET: " + String.format("%.10f", NEOETHUSDT - NEOUSD) + " Percentage: " + df.format(NETNEOETHUSDT));
        System.out.println("NEO -> BNB -> USDT: " + String.format("%.10f", NEOBNB.getPrice() * BNBUSDT.getPrice()) + " NET: " + String.format("%.10f", NEOBNBUSDT - NEOUSD) + " Percentage: " + df.format(NETNEOBNBUSDT));

        //System.out.println(gson.toJson(minUSDT));
        //System.out.println(gson.toJson(maxUSDT)) ;

        activity.showNeoUSDT(NEOUSDTTicker);
        activity.showNeoBtcUSDT(NEOBTCUSDTTicker);
        activity.showNeoEthUSDT(NEOETHUSDTTicker);
        activity.showNeoBnbUSDT(NEOBNBUSDTTicker);

        activity.showMaxUSDT(maxUSDT);
        activity.showMinUSDT(minUSDT);
        activity.showNeoNetGain(maxUSDT.getPrice()-minUSDT.getPrice());
        activity.showLog(transactionLog);

    }




}
