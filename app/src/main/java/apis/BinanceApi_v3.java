package apis;

import io.reactivex.Single;
import models.Binance.BookTicker;
import models.Binance.Ticker;
import models.Binance.Time;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Hardey on 13/01/2018.
 */

public interface BinanceApi_v3 {

    @GET("ticker/price")
    Single<Ticker[]> getAllTickers();

    @GET("ticker/price")
    Single<Ticker> getSingleTicker(@Query(value = "symbol") String symbol);

    @GET("ticker/bookTicker")
    Single<BookTicker[]> getAllBookTickers();
}
