package apis;

import io.reactivex.Single;
import models.Binance.FullResponse;
import models.Binance.Product;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by hardeylim on 26/01/2018.
 */

public interface BinanceApi_Trade {
        @GET("https://api.binance.com/api/v3/account")
        Single<String> getMyAccounts(
                @Query("timestamp") String timestamp
        );

//        @GET("https://api.binance.com/api/v1/ticker/allPrices")
//        Single<String> getAllPrices();


//        @GET("https://api.fixer.io/latest?base=USD")
//        Single<String> getExchange(
//                @Query("symbols") String symbols
//        );


        @POST("https://api.binance.com/api/v3/order/test?side=BUY&type=MARKET&newOrderRespType=FULL")
        Single<String> buyTest(
                @Query("timestamp") long timestamp,
                @Query("quantity") String quantity,
                @Query("symbol") String symbol
        );

        @POST("https://api.binance.com/api/v3/order?side=BUY&type=MARKET&newOrderRespType=FULL")
        Single<FullResponse> buy(
                @Query("timestamp") long timestamp,
                @Query("quantity") String quantity,
                @Query("symbol") String symbol
        );

        @POST("https://api.binance.com/api/v3/order?side=SELL&type=MARKET&newOrderRespType=FULL")
        Single<FullResponse> sell(
                @Query("timestamp") long timestamp,
                @Query("quantity") String quantity,
                @Query("symbol") String symbol
        );

        @POST("https://api.binance.com/api/v3/order/test?side=SELL&type=MARKET&newOrderRespType=FULL")
        Single<String> sellTest(
                @Query("timestamp") long timestamp,
                @Query("quantity") String quantity,
                @Query("symbol") String symbol
        );

        @GET("https://us.binance.com/exchange/public/product")
        Single<Product> getProduct();

}
