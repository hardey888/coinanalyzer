package apis;

import io.reactivex.Single;
import models.Coinsph.MarketDto;
import retrofit2.http.GET;

/**
 * Created by hardeylim on 05/01/2018.
 */

public interface CoinsPhApi {

    @GET("BTC-PHP")
    Single<MarketDto> peso();

    @GET("BTC-USD")
    Single<MarketDto> usd();

}
