package apis;

import org.json.JSONArray;
import org.json.JSONObject;

import io.reactivex.Single;
import models.Binance.Interval;
import java.util.List;
import models.Binance.KLine;
import models.Binance.KLines;
import models.Binance.Time;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Hardey on 13/01/2018.
 */

public interface BinanceApi_v1 {

    @GET("time")
    Single<Time> getServerTime();

    //@GET("depth")
    //Single<> getDepth(@Query("symbol") String symbol);

    @GET("klines")
    Single<String[][]> getKLines(@Query("symbol") String pair,
                                 @Query("interval") String interval,
                                 @Query("limit") int limit);


}
