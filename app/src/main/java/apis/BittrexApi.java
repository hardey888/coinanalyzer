package apis;

import io.reactivex.Single;
import models.Bittrex.AllCurrencies;
import models.Bittrex.AllMarkets;
import models.Bittrex.OrderBook;
import models.Coinsph.MarketDto;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by hardeylim on 05/01/2018.
 */

public interface BittrexApi {

    @GET("getmarkets")
    Single<AllMarkets> getMarkets();

    @GET("getcurrencies")
    Single<AllCurrencies> getCurrencies();

    @GET("getorderbook")
    Single<OrderBook> getOrderBook(@Query("market") String pair,
                                   @Query("type") String type);




}
