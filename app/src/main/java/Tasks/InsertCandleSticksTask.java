package Tasks;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import Database.CandleStickDatabase;
import models.Binance.CandleStick;

/**
 * Created by hardeylim on 23/01/2018.
 */

public class InsertCandleSticksTask extends AsyncTask<CandleStick , Integer, Long> {
    private Context mContext;
    CandleStickDatabase database = Room.databaseBuilder(getmContext(), CandleStickDatabase.class, "CandleSticks").build();


    protected Long doInBackground(CandleStick...  candleSticks) {
        database.binanceDao().insertMany(candleSticks);
        return null;
    }


    public InsertCandleSticksTask(){
        super();
    }

    public InsertCandleSticksTask(Context context){
        this.mContext = context;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }
}
