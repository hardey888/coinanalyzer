package Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import io.reactivex.Maybe;
import models.Binance.CandleStick;

/**
 * Created by hardeylim on 19/01/2018.
 */
@Dao
public interface BinanceDao {
    @Insert
    void insertOne(CandleStick candleStick);

    @Insert
    void insertMany(CandleStick... candleSticks);

    @Query("SELECT * FROM CandleStick WHERE pair = :pair")
    Maybe<CandleStick> getUserById(String pair);
}
