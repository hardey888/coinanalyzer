package Dao;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import models.Coinsph.MarketPrice;

/**
 * Created by Hardey on 07/01/2018.
 */

public interface CoinsPhDao {

    @Query("SELECT * FROM market_price")
    MarketPrice[] loadAllMarketPrices();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertMarketPrice(MarketPrice... marketPrice);

    @Update
    public void updateMarketPrice(MarketPrice... marketPrice);

    @Delete
    public void deleteUsers(MarketPrice... marketPrice);


}
