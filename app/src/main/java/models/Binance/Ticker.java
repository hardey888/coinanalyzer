package models.Binance;

/**
 * Created by hardeylim on 25/01/2018.
 */

public class Ticker {
    private String symbol;
    private double price;

    public Ticker() {
        super();
    }

    public Ticker(String symbol, double price) {
        this.symbol = symbol;
        this.price = price;
    }

    public void setTicker(String symbol, double price) {
        this.symbol = symbol;
        this.price = price;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
