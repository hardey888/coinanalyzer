package models.Binance;

import java.math.BigDecimal;

/**
 * Created by hardeylim on 05/02/2018.
 */

public class Trade {
    Symbol symbol;
    BigDecimal rate;
    Action action;

    public Trade() {
    }

    public Trade(Symbol symbol) {
        this.symbol = symbol;
    }

    public Trade(Symbol symbol, Action action) {
        this.symbol = symbol;
        this.action = action;
    }

    public Trade(Symbol symbol, BigDecimal rate, Action action) {
        this.symbol = symbol;
        this.rate = rate;
        this.action = action;
    }

    public void compute(BookTicker bookTicker){
        switch (this.action){
            case BUY:
                this.setRate(bookTicker.getAskPrice()); break;
            case SELL:
                this.setRate(bookTicker.getBidPrice()); break;
        }
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}
