package models.Binance;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;

import Utils.Utils;

/**
 * Created by hardeylim on 05/02/2018.
 */

public class ExecutionPath {

    BigDecimal netGainPercentage;
    BigDecimal netGain;

    LinkedList<Trade> trades = new LinkedList<>();

    //Computation
    BigDecimal startingValue;
    BigDecimal endingValue;
    BigDecimal divisibleInitialValue;
    String coinAfterExecution = "BTC"; // This is the Base currency to start with.
    String trailingBalance = "0.0"; //BTC
    String INITIAL_COIN = "BTC";

    public ExecutionPath() {
    }

    public ExecutionPath(LinkedList<Trade> trades) {
        this.trades = trades;
    }

    public void compute(){
        computeComplexEndingValue();
        computeComplexNetGain();
    }

    public void computeSilently(){
        computeComplexEndingValueSilently();
        computeComplexNetGain();
    }

    public void computeComplexNetGain(){
        this.netGain = this.endingValue.subtract(this.divisibleInitialValue);
        this.netGainPercentage = this.netGain.divide(this.divisibleInitialValue,8, RoundingMode.DOWN);
    }

    public void computeComplexEndingValue(){
        BigDecimal STARTING_AMOUNT = this.startingValue;

        String buySellQuantity = "0.0";
        Trade firstTrade = this.trades.getFirst();
        Trade secondTrade = this.trades.get(1);
        Trade lastTrade = this.trades.getLast();

        this.trailingBalance = this.startingValue.toString();
        buySellQuantity = Utils.roundBasedOnPair(firstTrade.getSymbol(), STARTING_AMOUNT.divide(firstTrade.getRate(),8, RoundingMode.HALF_DOWN));
        this.trailingBalance = buySellQuantity.toString();
        BigDecimal exactInitialValue = new BigDecimal(buySellQuantity).multiply(firstTrade.getRate());
        this.divisibleInitialValue = exactInitialValue;

        simulateFirstBuy(firstTrade.getSymbol().toString(), buySellQuantity, firstTrade.getSymbol());
        switch (secondTrade.getAction()) {
            case BUY:
                buySellQuantity = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).divide(secondTrade.getRate(), 8, RoundingMode.HALF_DOWN));
                trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).divide(secondTrade.getRate(),8, RoundingMode.HALF_DOWN));
                simulateSecondBuy(secondTrade.getSymbol().toString(), buySellQuantity, secondTrade.getSymbol());
                trailingBalance = new BigDecimal(trailingBalance).multiply(lastTrade.getRate()).toString();
                simulateLastSell(lastTrade.getSymbol().toString(), buySellQuantity, lastTrade.getSymbol());
                break;
            case SELL:
                trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).multiply(secondTrade.getRate()));
                simulateSecondSell(secondTrade.getSymbol().toString(), buySellQuantity, secondTrade.getSymbol());
                buySellQuantity = trailingBalance;
                trailingBalance = new BigDecimal(trailingBalance).multiply(lastTrade.getRate()).toString();
                simulateLastSell(lastTrade.getSymbol().toString(), buySellQuantity, lastTrade.getSymbol());
                break;
        }
        this.endingValue=new BigDecimal(trailingBalance);
        System.out.println("Initial Deducted Balance" + exactInitialValue);
    }

    public void computeComplexEndingValueSilently(){
        BigDecimal STARTING_AMOUNT = this.startingValue;

        String buySellQuantity = "0.0";
        Trade firstTrade = this.trades.getFirst();
        Trade secondTrade = this.trades.get(1);
        Trade lastTrade = this.trades.getLast();

        this.trailingBalance = this.startingValue.toString();
        buySellQuantity = Utils.roundBasedOnPair(firstTrade.getSymbol(), STARTING_AMOUNT.divide(firstTrade.getRate(),8, RoundingMode.HALF_DOWN));
        this.trailingBalance = buySellQuantity.toString();
        BigDecimal exactInitialValue = new BigDecimal(buySellQuantity).multiply(firstTrade.getRate());
        this.divisibleInitialValue = exactInitialValue;

        switch (secondTrade.getAction()) {
            case BUY:
                buySellQuantity = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).divide(secondTrade.getRate(), 8, RoundingMode.HALF_DOWN));
                trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).divide(secondTrade.getRate(),8, RoundingMode.HALF_DOWN));
                trailingBalance = new BigDecimal(trailingBalance).multiply(lastTrade.getRate()).toString();
                break;
            case SELL:
                trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).multiply(secondTrade.getRate()));
                buySellQuantity = trailingBalance;
                trailingBalance = new BigDecimal(trailingBalance).multiply(lastTrade.getRate()).toString();
                break;
        }
        this.endingValue=new BigDecimal(trailingBalance);
    }

    private void simulateFirstBuy(String pair, String quantity, Symbol tradeSymbol){
        coinAfterExecution = pair.toString().replace(INITIAL_COIN,"");
        System.out.println("Trade Execution BUY " + tradeSymbol + " Quantity: " + quantity + " " + coinAfterExecution);
        System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
    }

    private void simulateSecondBuy(String pair, String quantity, Symbol tradeSymbol){
        coinAfterExecution = pair.replace(coinAfterExecution,"");
        System.out.println("Trade Execution BUY " + tradeSymbol + " Quantity: " + quantity + " " + coinAfterExecution);
        System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
    }

    private void simulateSecondSell(String pair, String quantity, Symbol tradeSymbol){
        System.out.println("Trade Execution SELL " + tradeSymbol + " Quantity: " + quantity + " " + coinAfterExecution);
        coinAfterExecution = pair.replace(coinAfterExecution,"");
        System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
    }

    private void simulateLastSell(String pair, String quantity, Symbol tradeSymbol){
        System.out.println("Trade Execution SELL " + tradeSymbol + " Quantity: " + quantity + " " + coinAfterExecution);
        coinAfterExecution = pair.replace(coinAfterExecution,"");
        System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
    }



    public ExecutionPath(BigDecimal startingValue, LinkedList<Trade> trades, BigDecimal endingValue, BigDecimal netGain, double netGainPercentage) {
        this.startingValue = startingValue;
        this.trades = trades;
        this.endingValue = endingValue;
        this.netGain = netGain;
        //this.netGainPercentage = netGainPercentage;
    }

    public BigDecimal getStartingValue() {
        return startingValue;
    }

    public void setStartingValue(BigDecimal startingValue) {
        this.startingValue = startingValue;
    }

    public LinkedList<Trade> getTrades() {
        return trades;
    }

    public void setTrades(LinkedList<Trade> trades) {
        this.trades = trades;
    }

    public BigDecimal getEndingValue() {
        return endingValue;
    }

    public void setEndingValue(BigDecimal endingValue) {
        this.endingValue = endingValue;
    }

    public BigDecimal getNetGain() {
        return netGain;
    }

    public void setNetGain(BigDecimal netGain) {
        this.netGain = netGain;
    }

//    public double getNetGainPercentage() {
//        return netGainPercentage;
//    }
//
//    public void setNetGainPercentage(double netGainPercentage) {
//        this.netGainPercentage = netGainPercentage;
//    }

    public BigDecimal getDivisibleInitialValue() {
        return divisibleInitialValue;
    }

    public void setDivisibleInitialValue(BigDecimal divisibleInitialValue) {
        this.divisibleInitialValue = divisibleInitialValue;
    }

    public BigDecimal getNetGainPercentage() {
        return netGainPercentage;
    }

    public void setNetGainPercentage(BigDecimal netGainPercentage) {
        this.netGainPercentage = netGainPercentage;
    }
}
