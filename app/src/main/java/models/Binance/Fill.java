package models.Binance;

/**
 * Created by hardeylim on 29/01/2018.
 */

public class Fill {
    private double price;
    private double qty;
    private double commission;
    private String commissionAsset;

    public Fill() {
    }

    public Fill(double price, double qty, double commission, String commissionAsset) {
        this.price = price;
        this.qty = qty;
        this.commission = commission;
        this.commissionAsset = commissionAsset;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public String getCommissionAsset() {
        return commissionAsset;
    }

    public void setCommissionAsset(String commissionAsset) {
        this.commissionAsset = commissionAsset;
    }
}
