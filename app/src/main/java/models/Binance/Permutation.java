package models.Binance;

import java.util.List;
import java.util.Map;

/**
 * Created by hardeylim on 05/02/2018.
 */

public class Permutation {
    String gateway;
    List<Map<String,String>> paths;

    public Permutation(String gateway, List<Map<String, String>> paths) {
        this.gateway = gateway;
        this.paths = paths;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public List<Map<String, String>> getPaths() {
        return paths;
    }

    public void setPaths(List<Map<String, String>> paths) {
        this.paths = paths;
    }
}
