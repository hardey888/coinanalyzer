package models.Binance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hardeylim on 29/01/2018.
 */

public class FullResponse {
    private String symbol;
    private int orderId;
    private String clientOrderId;
    private long transactTime;
    private double price;
    private double origQty;
    private double executedQty;
    private String status;
    private String timeInForce;
    private String type;
    private String side;
    private List<Fill> fills = new ArrayList<Fill>();

    public FullResponse() {
    }

    public FullResponse(String symbol, int orderId, String clientOrderId, long transactTime, double price, double origQty, double executedQty, String status, String timeInForce, String type, String side, List<Fill> fills) {
        this.symbol = symbol;
        this.orderId = orderId;
        this.clientOrderId = clientOrderId;
        this.transactTime = transactTime;
        this.price = price;
        this.origQty = origQty;
        this.executedQty = executedQty;
        this.status = status;
        this.timeInForce = timeInForce;
        this.type = type;
        this.side = side;
        this.fills = fills;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getClientOrderId() {
        return clientOrderId;
    }

    public void setClientOrderId(String clientOrderId) {
        this.clientOrderId = clientOrderId;
    }

    public long getTransactTime() {
        return transactTime;
    }

    public void setTransactTime(long transactTime) {
        this.transactTime = transactTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getOrigQty() {
        return origQty;
    }

    public void setOrigQty(double origQty) {
        this.origQty = origQty;
    }

    public double getExecutedQty() {
        return executedQty;
    }

    public void setExecutedQty(double executedQty) {
        this.executedQty = executedQty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeInForce() {
        return timeInForce;
    }

    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public List<Fill> getFills() {
        return fills;
    }

    public void setFills(List<Fill> fills) {
        this.fills = fills;
    }
}
