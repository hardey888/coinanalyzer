package models.Binance;

/**
 * Created by hardeylim on 08/02/2018.
 */

public class Detail {

    private String quoteAsset;

    private String activeBuy;

    private String prevClose;

    private String withdrawFee;

    private String symbol;

    private String status;

    private String baseAssetName;

    private String minQty;

    private String minTrade;

    private String baseAssetUnit;

    private String quoteAssetUnit;

    private String decimalPlaces;

    private String activeSell;

    private String close;

    private String lastAggTradeId;

    private String open;

    private String baseAsset;

    private String quoteAssetName;

    private String volume;

    private String active;

    private String tradedMoney;

    private String high;

    private String low;

    private String matchingUnitType;

    private String tickSize;

    public String getQuoteAsset() {
        return quoteAsset;
    }

    public void setQuoteAsset(String quoteAsset) {
        this.quoteAsset = quoteAsset;
    }

    public String getActiveBuy() {
        return activeBuy;
    }

    public void setActiveBuy(String activeBuy) {
        this.activeBuy = activeBuy;
    }

    public String getPrevClose() {
        return prevClose;
    }

    public void setPrevClose(String prevClose) {
        this.prevClose = prevClose;
    }

    public String getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(String withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBaseAssetName() {
        return baseAssetName;
    }

    public void setBaseAssetName(String baseAssetName) {
        this.baseAssetName = baseAssetName;
    }

    public String getMinQty() {
        return minQty;
    }

    public void setMinQty(String minQty) {
        this.minQty = minQty;
    }

    public String getMinTrade() {
        return minTrade;
    }

    public void setMinTrade(String minTrade) {
        this.minTrade = minTrade;
    }

    public String getBaseAssetUnit() {
        return baseAssetUnit;
    }

    public void setBaseAssetUnit(String baseAssetUnit) {
        this.baseAssetUnit = baseAssetUnit;
    }

    public String getQuoteAssetUnit() {
        return quoteAssetUnit;
    }

    public void setQuoteAssetUnit(String quoteAssetUnit) {
        this.quoteAssetUnit = quoteAssetUnit;
    }

    public String getDecimalPlaces() {
        return decimalPlaces;
    }

    public void setDecimalPlaces(String decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

    public String getActiveSell() {
        return activeSell;
    }

    public void setActiveSell(String activeSell) {
        this.activeSell = activeSell;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getLastAggTradeId() {
        return lastAggTradeId;
    }

    public void setLastAggTradeId(String lastAggTradeId) {
        this.lastAggTradeId = lastAggTradeId;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getBaseAsset() {
        return baseAsset;
    }

    public void setBaseAsset(String baseAsset) {
        this.baseAsset = baseAsset;
    }

    public String getQuoteAssetName() {
        return quoteAssetName;
    }

    public void setQuoteAssetName(String quoteAssetName) {
        this.quoteAssetName = quoteAssetName;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getTradedMoney() {
        return tradedMoney;
    }

    public void setTradedMoney(String tradedMoney) {
        this.tradedMoney = tradedMoney;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getMatchingUnitType() {
        return matchingUnitType;
    }

    public void setMatchingUnitType(String matchingUnitType) {
        this.matchingUnitType = matchingUnitType;
    }

    public String getTickSize() {
        return tickSize;
    }

    public void setTickSize(String tickSize) {
        this.tickSize = tickSize;
    }
}
