package models.Binance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hardeylim on 08/02/2018.
 */

public class Product {
    private List<Detail> data;

    public Product() {
        data = new ArrayList<>();
    }

    public List<Detail> getData() {
        return data;
    }

    public void setData(List<Detail> data) {
        this.data = data;
    }
}
