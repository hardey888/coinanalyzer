package models.Binance;

import java.util.Date;

/**
 * Created by hardeylim on 26/01/2018.
 */

public class TransactionLog {
    Ticker max;
    Ticker min;
    double netGain;
    double percentageGain;
    double timestamp;
    Date dateTime;

    public TransactionLog() {
        this.max = new Ticker();
        this.min = new Ticker();
        this.netGain = 0;

    }

    public TransactionLog(Ticker max, models.Binance.Ticker min, double netGain, double timestamp, Date dateTime) {
        this.max = max;
        this.min = min;
        this.netGain = netGain;
        this.timestamp = timestamp;
        this.dateTime = dateTime;
    }

    public void store(Ticker max, Ticker min, double netGain, double percentageGain, double timestamp, Date dateTime) {
        this.max = max;
        this.min = min;
        this.netGain = netGain;
        this.percentageGain = percentageGain;
        this.timestamp = timestamp;
        this.dateTime = dateTime;
    }

    public Ticker getMax() {
        return max;
    }

    public void setMax(Ticker max) {
        this.max = max;
    }

    public Ticker getMin() {
        return min;
    }

    public void setMin(Ticker min) {
        this.min = min;
    }

    public double getNetGain() {
        return netGain;
    }

    public void setNetGain(double netGain) {
        this.netGain = netGain;
    }

    public double getPercentageGain() {
        return percentageGain;
    }

    public void setPercentageGain(double percentageGain) {
        this.percentageGain = percentageGain;
    }

    public double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
