package models.Global;

/**
 * Created by hardeylim on 26/01/2018.
 */

public class Globals {
    private static String apiKey = "";
    private static String secretKey = "";

    public static String getApiKey() {
        return apiKey;
    }

    public static String getSecretKey() {
        return secretKey;
    }
}

