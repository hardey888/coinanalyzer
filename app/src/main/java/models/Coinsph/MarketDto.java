package models.Coinsph;


public class MarketDto {

    private MarketPrice market;

    public MarketPrice getMarket() {
        return market;
    }

    public void setMarket(MarketPrice market) {
        this.market = market;
    }
}
