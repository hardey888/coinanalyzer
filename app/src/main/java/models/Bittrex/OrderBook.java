package models.Bittrex;

import android.os.Build;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by hardeylim on 08/01/2018.
 */

public class OrderBook {
    private boolean success;
    private String message;
    private OrderResult result;

    public class OrderResult {
        private List<Order> buy = new ArrayList<>();
        private List<Order> sell = new ArrayList<>();

        public List<Order> getBuy() {
            return buy;
        }

        public void setBuy(List<Order> buy) {
            this.buy = buy;
        }

        public List<Order> getSell() {
            return sell;
        }

        public void setSell(List<Order> sell) {
            this.sell = sell;
        }

        public double getBuyTotal(){
            return this.buy.stream().mapToDouble(Order::getQuantity).sum();
        }

        public double getSellTotal(){
            return this.sell.stream().mapToDouble(Order::getQuantity).sum();
        }

    }

    public class Order {
        @SerializedName("Quantity")
        private double quantity;
        @SerializedName("Rate")
        private double rate;

        public double getQuantity() {
            return quantity;
        }

        public void setQuantity(double quantity) {
            this.quantity = quantity;
        }

        public double getRate() {
            return rate;
        }

        public void setRate(double rate) {
            this.rate = rate;
        }

    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrderResult getResult() {
        return result;
    }

    public void setResult(OrderResult result) {
        this.result = result;
    }
}
