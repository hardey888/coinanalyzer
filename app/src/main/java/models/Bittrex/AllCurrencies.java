package models.Bittrex;

import java.util.List;

/**
 * Created by hardeylim on 08/01/2018.
 */

public class AllCurrencies {
    private String success;
    private String message;
    private List<Currency> result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Currency> getResult() {
        return result;
    }

    public void setResult(List<Currency> result) {
        this.result = result;
    }
}
