package models.Bittrex;

import java.util.List;

/**
 * Created by Hardey on 06/01/2018.
 */

public class AllMarkets {

    private String success;
    private String message;
    private List<Market> result;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Market> getResult() {
        return result;
    }

    public void setResult(List<Market> result) {
        this.result = result;
    }
}


