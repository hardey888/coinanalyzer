package models.Bittrex;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hardeylim on 08/01/2018.
 */

public class Currency {

    @SerializedName("Currency")
    private String currency;
    @SerializedName("CurrencyLong")
    private String currencyLong;
    @SerializedName("MinConfirmation")
    private String minConfirmation;
    @SerializedName("TxFee")
    private String txFee;
    @SerializedName("IsActive")
    private boolean isActive;
    @SerializedName("CoinType")
    private String coinType;
    @SerializedName("BaseAddress")
    private String baseAddress;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyLong() {
        return currencyLong;
    }

    public void setCurrencyLong(String currencyLong) {
        this.currencyLong = currencyLong;
    }

    public String getMinConfirmation() {
        return minConfirmation;
    }

    public void setMinConfirmation(String minConfirmation) {
        this.minConfirmation = minConfirmation;
    }

    public String getTxFee() {
        return txFee;
    }

    public void setTxFee(String txFee) {
        this.txFee = txFee;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getBaseAddress() {
        return baseAddress;
    }

    public void setBaseAddress(String baseAddress) {
        this.baseAddress = baseAddress;
    }
}
