package models.Bittrex;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hardey on 06/01/2018.
 */

public class Market {
    @SerializedName("MarketCurrency")
    private String marketCurrency;
    @SerializedName("BaseCurrency")
    private String baseCurrency;
    @SerializedName("MarketCurrencyLong")
    private String marketCurrencyLong;
    @SerializedName("BaseCurrencyLong")
    private String baseCurrencyLong;
    @SerializedName("MinTradeSize")
    private float minTradeSize;
    @SerializedName("MarketName")
    private String marketName;
    @SerializedName("IsActive")
    private boolean isActive;
    @SerializedName("Created")
    private String createdDate;

    public String getMarketCurrency() {
        return marketCurrency;
    }

    public void setMarketCurrency(String marketCurrency) {
        this.marketCurrency = marketCurrency;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public String getMarketCurrencyLong() {
        return marketCurrencyLong;
    }

    public void setMarketCurrencyLong(String marketCurrencyLong) {
        this.marketCurrencyLong = marketCurrencyLong;
    }

    public String getBaseCurrencyLong() {
        return baseCurrencyLong;
    }

    public void setBaseCurrencyLong(String baseCurrencyLong) {
        this.baseCurrencyLong = baseCurrencyLong;
    }

    public float getMinTradeSize() {
        return minTradeSize;
    }

    public void setMinTradeSize(float minTradeSize) {
        this.minTradeSize = minTradeSize;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
