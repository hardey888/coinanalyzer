package Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import Dao.BinanceDao;
import models.Binance.CandleStick;

@Database(entities = {CandleStick.class}, version = 1)
public abstract class CandleStickDatabase extends RoomDatabase {
    public abstract BinanceDao binanceDao();
}
