package com.example.hardeylim.coinanalyzer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import Presenter.NeoWatcherPresenter;
import models.Binance.Ticker;
import models.Binance.TransactionLog;

public class MainActivity extends AppCompatActivity {

    private DecimalFormat percentFormat = new DecimalFormat("##.##%");

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.linear_main);

        Toast.makeText(this, "", Toast.LENGTH_LONG).show();

        //CandleStickDatabase database = Room.databaseBuilder(getApplicationContext(), CandleStickDatabase.class, "CandleSticks").build();
        //InsertCandleSticksTask insertCandleSticksTask = new InsertCandleSticksTask(getApplicationContext());
        //CandleStick candleStick = new CandleStick();
        //candleStick.setPair("VENBTC");

        //database.binanceDao().insertOne(candleStick);

        //insertCandleSticksTask.execute(candleStick);
        TextView neoHeader = findViewById(R.id.NeoHeader);
        neoHeader.setText("NEO");



        NeoWatcherPresenter neoWatcherPresenter = new NeoWatcherPresenter(this);

        neoWatcherPresenter.watchNeoPairs();



    }



    public void showNeoUSDT(Ticker ticker){
        TextView neoUSDTLabel = findViewById(R.id.NEOUSDT);
        neoUSDTLabel.setText("NEOUSDT: " + formatDouble(ticker.getPrice()));
    }

    public void showNeoBtcUSDT(Ticker ticker){
        TextView neoBTCUSDT = findViewById(R.id.NEOBTCUSDT);
        neoBTCUSDT.setText("NEO -> BTC -> USDT : " + formatDouble(ticker.getPrice()));
    }

    public void showNeoEthUSDT(Ticker ticker){
        TextView neoETHUSDT = findViewById(R.id.NEOETHUSDT);
        neoETHUSDT.setText("NEO -> ETH -> USDT : ");
    }

    public void showNeoBnbUSDT(Ticker ticker){
        TextView neoBNBUSDT = findViewById(R.id.NEOBNBUSDT);
        neoBNBUSDT.setText("NEO -> BNB -> USDT : ");
    }

    public void showMaxUSDT(Ticker ticker){
        TextView maxUSDT = findViewById(R.id.MAXUSDT);
        maxUSDT.setText("MAXIMUM " + ticker.getSymbol()+ ": " + ticker.getPrice());
    }

    public void showMinUSDT(Ticker ticker){
        TextView maxUSDT = findViewById(R.id.MINUSDT);
        maxUSDT.setText("MINIMUM " + ticker.getSymbol()+ ": " + ticker.getPrice());
    }

    public void showNeoNetGain(Double netGain){
        TextView neoNetGain = findViewById(R.id.NEONETGAIN);
        neoNetGain.setText("Neo Net Gain: " + formatDouble(netGain));

    }

    public void showLog(TransactionLog transactionLog){
        TextView savedMax = findViewById(R.id.SavedMax);
        TextView savedMin = findViewById(R.id.SavedMin);
        TextView savedNetGain = findViewById(R.id.SavedNetGain);
        TextView savedDateTime = findViewById(R.id.SaveDateTime);

        savedMax.setText("Saved Max: " + transactionLog.getMax().getSymbol() + " :" + transactionLog.getMax().getPrice());
        savedMin.setText("Saved Min: " + transactionLog.getMin().getSymbol() + " :" + transactionLog.getMin().getPrice());
        savedNetGain.setText("Saved Net Gain: " + formatDouble(transactionLog.getNetGain()) + "Percentage: " + percentFormat.format(transactionLog.getPercentageGain()));
        savedDateTime.setText("Date Time: " + transactionLog.getDateTime());
    }

    private String formatDouble(Double input){
        NumberFormat formatter = new DecimalFormat("#0.0000");
        return formatter.format(input);
    }

}
