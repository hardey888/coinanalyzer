package Utils;

import android.text.format.DateUtils;

import com.google.gson.Gson;

import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import models.Binance.Action;
import models.Binance.BookTicker;
import models.Binance.Detail;
import models.Binance.ExecutionPath;
import models.Binance.Product;
import models.Binance.Symbol;
import models.Binance.Ticker;
import models.Binance.Trade;
import models.Global.Globals;

/**
 * Created by hardeylim on 26/01/2018.
 */

public class Utils {

    public static List<ExecutionPath> populateExecutionPaths(List<ExecutionPath> executionPaths, List<BookTicker> tickers, BigDecimal startingAmount){
        for(ExecutionPath executionPath : executionPaths){
            executionPath.setStartingValue(startingAmount);
            executionPath.setEndingValue(startingAmount);
            LinkedList<Trade> trades = executionPath.getTrades();
            for(Trade trade : trades){
                Symbol symbol = trade.getSymbol();
                BookTicker ticker = tickers.stream().filter(bookTicker -> bookTicker.getSymbol()
                        .equals(symbol.toString())).findFirst().get();
                trade.compute(ticker);
            }
            executionPath.compute();
        }
        return executionPaths;
    }

    public static List<ExecutionPath> populateExecutionPathsSilently(List<ExecutionPath> executionPaths, List<BookTicker> tickers, BigDecimal startingAmount){
        for(ExecutionPath executionPath : executionPaths){
            executionPath.setStartingValue(startingAmount);
            executionPath.setEndingValue(startingAmount);
            LinkedList<Trade> trades = executionPath.getTrades();
            for(Trade trade : trades){
                Symbol symbol = trade.getSymbol();
                BookTicker ticker = tickers.stream().filter(bookTicker -> bookTicker.getSymbol()
                        .equals(symbol.toString())).findFirst().get();
                trade.compute(ticker);
            }
            executionPath.computeSilently();
        }
        return executionPaths;
    }


    public static List<ExecutionPath> findBTCTriangles(List<BookTicker> inputBookTickers){
        System.out.println("=========Removing VIBE/VIB Tickers=========");
        System.out.println("=========Removing IOST/OST Tickers=========");
        System.out.println("=========Removing USDT     Tickers=========");
        List<ExecutionPath> executionPaths = new ArrayList<>();
        Gson gson = new Gson();
        List<LinkedList<Symbol>> tradeList = new ArrayList<>();
        Set<String> output = new HashSet<>();
        List<BookTicker> tickers = inputBookTickers.stream()
                .filter(ticker -> !ticker.getSymbol().contains("VIB"))
                .filter(ticker -> !ticker.getSymbol().contains("OST"))
                .filter(ticker -> !ticker.getSymbol().contains("USDT"))
                .collect(Collectors.toList());

        System.out.println("=========Finding Bitcoin Triangles=========");
        List<String> pairsEndingInBTC = tickers.stream()
                                      .map(ticker -> ticker.getSymbol())
                                      .filter(s -> s.endsWith("BTC"))
                                      .collect(Collectors.toList());

        for (String pair : pairsEndingInBTC){
            String symbol = pair.replace("BTC","");
            List<String> availablePairs = tickers.stream()
                    .map(ticker -> ticker.getSymbol())
                    .filter(s -> s.startsWith(symbol) || s.endsWith(symbol))
                    .filter(s -> !s.contains("BTC"))
                    .collect(Collectors.toList());
            //System.out.println(availablePairs);
            availablePairs.stream().forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    Trade trade = new Trade();
                    LinkedList<Symbol> tradeLinkedList = new LinkedList<>();
                    String firstPair = pair;
                    String secondPair = s;
                    String thirdPair = String.format("%s%s",s.replace(symbol,""),"BTC");
                    //System.out.println("First  Pair: " + firstPair);
                    //System.out.println("Second Pair: "+ secondPair);
                    //System.out.println("Third  Pair: " + thirdPair);
                    tradeLinkedList.add(Symbol.valueOf(firstPair));
                    tradeLinkedList.add(Symbol.valueOf(secondPair));
                    tradeLinkedList.add(Symbol.valueOf(thirdPair));
                    tradeList.add(tradeLinkedList);

                    ExecutionPath executionPath = mapSymbolsToTrade(firstPair, secondPair, thirdPair, inputBookTickers);
                    executionPaths.add(executionPath);
                }
            });
        }

        //System.out.println("Symbols ending in BTC: " + pairsEndingInBTC);
        //System.out.println("Output Size " + output.size());
        //System.out.println("Output : " + output);
        //System.out.println("Trade Linked List : " + tradeList);
        //System.out.println("Execution Paths: " + gson.toJson(executionPaths));
        return executionPaths;
    }

    private static ExecutionPath mapSymbolsToTrade(String firstPair, String secondPair, String thirdPair, List<BookTicker> bookTickers){
        LinkedList<Trade> trades = new LinkedList<>();
        BookTicker firstTicker = bookTickers.stream()
                                .filter(bookTicker -> bookTicker.getSymbol().equals(firstPair))
                                .findFirst()
                                .get();
        BookTicker secondTicker = bookTickers.stream()
                                .filter(bookTicker -> bookTicker.getSymbol().equals(secondPair))
                                .findFirst()
                                .get();
        BookTicker thirdTicker = bookTickers.stream()
                                .filter(bookTicker -> bookTicker.getSymbol().equals(thirdPair))
                                .findFirst()
                                .get();
        String firstPairRemovedBTC = firstPair.replace("BTC","");
        Action secondAction = secondPair.endsWith(firstPairRemovedBTC) ? Action.BUY : Action.SELL;

        Trade firstTrade = new Trade(Symbol.valueOf(firstPair), firstTicker.getAskPrice(), Action.BUY);
        Trade secondTrade = new Trade(Symbol.valueOf(secondPair), secondTicker.getAskPrice(), secondAction);
        Trade thirdTrade = new Trade(Symbol.valueOf(thirdPair), thirdTicker.getBidPrice(), Action.SELL);
        trades.add(firstTrade);
        trades.add(secondTrade);
        trades.add(thirdTrade);
        ExecutionPath executionPath = new ExecutionPath(trades);

        return executionPath;
    }

    public static List<String> checkForNewTickers(List<Ticker> tickers){
        System.out.println("=========Checking for new Tickers=========");
        List<String> enumSymbols  = Arrays.asList(Symbol.values()).stream().map(symbol -> symbol.toString()).collect(Collectors.toList());
        List<String> inputSymbols = tickers.stream().map(ticker -> ticker.getSymbol())
                                                    .collect(Collectors.toList());

        inputSymbols.removeAll(enumSymbols);
        inputSymbols.stream().filter(s -> !s.matches(".*\\d+.*"))
                             .forEach(s -> System.out.println(s + ","));
        return inputSymbols;
    }

    public static List<String> checkForNewBookTickers(List<BookTicker> bookTickers){
        System.out.println("=========Checking for new Tickers=========");
        List<String> enumSymbols  = Arrays.asList(Symbol.values()).stream().map(symbol -> symbol.toString()).collect(Collectors.toList());
        List<String> inputSymbols = bookTickers.stream().map(bookTicker -> bookTicker.getSymbol())
                .collect(Collectors.toList());

        inputSymbols.removeAll(enumSymbols);
        inputSymbols.stream().filter(s -> !s.matches(".*\\d+.*"))
                .forEach(s -> System.out.println(s + ","));
        return inputSymbols;
    }

    public static String getSignature(String data)  {
        String key = Globals.getSecretKey();
        try {

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            return new String(Hex.encodeHex(sha256_HMAC.doFinal(data.getBytes("UTF-8"))));

        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }



    public static String getTimeAgo(String time){

        if(!Objects.equals(time, "")){
            return String.valueOf(
                    DateUtils.getRelativeTimeSpanString(
                            Long.parseLong(time),
                            System.currentTimeMillis() ,
                            DateUtils.MINUTE_IN_MILLIS
                    )
            );
        }else{
            return null;
        }

    }

    public static String getDecimal(double d, int place){
        return String.valueOf(new BigDecimal(d).setScale(place, RoundingMode.HALF_EVEN));
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    /**
     * @deprecated
     * Double should not be used as it does not represent a number in exact amount.
     */
    @Deprecated
    public static String roundBasedOnPair(Symbol symbol, double value){

        switch(symbol){
            case NULSBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case VENBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BNBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NULSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CTRBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case NEOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NULSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case LINKBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case SALTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case IOTABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ETCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ASTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case KNCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case WTCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case SNGLSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case EOSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case SNTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case MCOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BTCUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000000").format(round(value,6)));
                return new DecimalFormat("#0.000000").format(round(value,6));
            case OAXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case OMGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case GASBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BQXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case WTCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case QTUMETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BNTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case DNTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ICNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case SNMBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case SNMETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case SNGLSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BQXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case NEOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case KNCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case STRATETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ZRXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case QTUMBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case FUNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case LTCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case LINKETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ETHBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case XVGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case STRATBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ZRXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case IOTAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BCCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case CTRETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case OMGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case MCOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case SALTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ADABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ADAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ADXBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ADXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ADXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case AEBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case AEBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case AEETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case AIONBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case AIONBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case AIONETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case AMBBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case AMBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case AMBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case APPCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case APPCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case APPCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ARKBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ARKETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ARNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ARNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ASTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BATBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BATBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BATETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BCCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return new DecimalFormat("#0.00000").format(round(value,5));
            case BCCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case BCCUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return new DecimalFormat("#0.00000").format(round(value,5));
            case BCDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case BCDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case BCPTBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BCPTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BCPTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BLZBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BLZBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BLZETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BNBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BNTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BRDBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BRDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BRDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BTGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BTGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BTSBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case BTSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case BTSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CDTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CDTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CHATBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CHATETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CMTBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case CMTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CMTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CNDBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case CNDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case CNDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case DASHBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case DASHETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case DGDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case DGDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case DLTBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case DLTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case DLTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case DNTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case EDOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case EDOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ELFBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ELFETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ENGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ENGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ENJBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ENJETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case EOSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ETCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case EVXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case EVXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case FUELBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case FUELETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case FUNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case GTOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case GTOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case GTOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case GVTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case GVTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case GXSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case GXSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case HSRBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case HSRETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ICNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ICXBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ICXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ICXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case INSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case INSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case IOSTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case IOSTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case IOTABNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case KMDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case KMDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case LENDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case LENDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case LRCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case LRCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case LSKBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case LSKBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case LSKETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case LTCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return new DecimalFormat("#0.00000").format(round(value,5));
            case LTCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case LTCUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return new DecimalFormat("#0.00000").format(round(value,5));
            case LUNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case LUNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case MANABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case MANAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case MCOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case MDABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case MDAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case MODBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case MODETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case MTHBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case MTHETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case MTLBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case MTLETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NANOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NANOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NANOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NAVBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NAVBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NAVETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NEBLBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NEBLBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NEBLETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case NEOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case NEOUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case OAXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case OSTBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case OSTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case OSTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case PIVXBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case PIVXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case PIVXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case POEBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case POEETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case POWRBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case POWRBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case POWRETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case PPTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case PPTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case QSPBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case QSPBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case QSPETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case RCNBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case RCNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case RCNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case RDNBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case RDNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case RDNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case REQBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case REQETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case RLCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case RLCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case RLCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case RPXBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case RPXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case RPXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case SNTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case STEEMBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case STEEMBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case STEEMETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case STORJBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case STORJETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case SUBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case SUBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case TNBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case TNBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case TNTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case TNTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case TRIGBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case TRIGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case TRIGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case TRXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case TRXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case VENBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case VENETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case VIABNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case VIABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case VIAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case VIBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case VIBEBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case VIBEETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case VIBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case WABIBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case WABIBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case WABIETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case WAVESBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case WAVESBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case WAVESETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case WINGSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case WINGSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case WTCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case XLMBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case XLMBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case XLMETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case XMRBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case XMRETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case XRPBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case XRPETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case XVGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case XZCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case XZCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case XZCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case YOYOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case YOYOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case YOYOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,1)));
                return new DecimalFormat("#0").format(round(value,1));
            case ZECBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case ZECETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case BNBUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return new DecimalFormat("#0.00").format(round(value,2));
            case ETHUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return new DecimalFormat("#0.00000").format(round(value,5));
            default: return "0.0";
        }
    }

    public static String roundBasedOnPair(Symbol symbol, BigDecimal value){

        switch(symbol){
            case NULSBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case VENBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BNBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NULSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CTRBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case NEOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NULSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case LINKBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case SALTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case IOTABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ETCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ASTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case KNCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case WTCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case SNGLSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case EOSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case SNTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case MCOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BTCUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000000").format(round(value,6)));
                return value.setScale(6,RoundingMode.DOWN).toString();
            case OAXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case OMGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case GASBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BQXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case WTCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case QTUMETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BNTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case DNTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ICNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case SNMBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case SNMETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case SNGLSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BQXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case NEOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case KNCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case STRATETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ZRXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case QTUMBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case FUNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case LTCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case LINKETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ETHBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case XVGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case STRATBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ZRXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case IOTAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BCCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case CTRETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case OMGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case MCOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case SALTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ADABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ADAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ADXBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ADXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ADXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case AEBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case AEBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case AEETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case AIONBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case AIONBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case AIONETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case AMBBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case AMBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case AMBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case APPCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case APPCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case APPCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ARKBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ARKETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ARNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ARNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ASTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BATBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BATBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BATETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BCCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return value.setScale(5,RoundingMode.DOWN).toString();
            case BCCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case BCCUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return value.setScale(5,RoundingMode.DOWN).toString();
            case BCDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case BCDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case BCPTBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BCPTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BCPTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BLZBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BLZBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BLZETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BNBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BNTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BRDBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BRDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BRDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BTGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BTGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BTSBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case BTSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case BTSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CDTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CDTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CHATBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CHATETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CMTBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case CMTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CMTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CNDBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case CNDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case CNDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case DASHBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case DASHETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case DGDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case DGDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case DLTBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case DLTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case DLTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case DNTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case EDOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case EDOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ELFBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ELFETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ENGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ENGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ENJBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ENJETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case EOSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ETCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case EVXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case EVXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case FUELBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case FUELETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case FUNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case GTOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case GTOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case GTOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case GVTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case GVTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case GXSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case GXSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case HSRBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case HSRETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ICNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ICXBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ICXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ICXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case INSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case INSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case IOSTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case IOSTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case IOTABNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case KMDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case KMDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case LENDBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case LENDETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case LRCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case LRCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case LSKBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case LSKBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case LSKETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case LTCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return value.setScale(5,RoundingMode.DOWN).toString();
            case LTCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case LTCUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return value.setScale(5,RoundingMode.DOWN).toString();
            case LUNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case LUNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case MANABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case MANAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case MCOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case MDABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case MDAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case MODBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case MODETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case MTHBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case MTHETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case MTLBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case MTLETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NANOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NANOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NANOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NAVBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NAVBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NAVETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NEBLBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NEBLBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NEBLETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case NEOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case NEOUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case OAXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case OSTBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case OSTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case OSTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case PIVXBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case PIVXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case PIVXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case POEBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case POEETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case POWRBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case POWRBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case POWRETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case PPTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case PPTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case QSPBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case QSPBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case QSPETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case RCNBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case RCNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case RCNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case RDNBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case RDNBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case RDNETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case REQBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case REQETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case RLCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case RLCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case RLCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case RPXBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case RPXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case RPXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case SNTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case STEEMBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case STEEMBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case STEEMETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case STORJBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case STORJETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case SUBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case SUBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case TNBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case TNBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case TNTBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case TNTETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case TRIGBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case TRIGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case TRIGETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case TRXBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case TRXETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case VENBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case VENETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case VIABNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case VIABTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case VIAETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case VIBBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case VIBEBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case VIBEETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case VIBETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case WABIBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case WABIBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case WABIETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case WAVESBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case WAVESBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case WAVESETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case WINGSBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case WINGSETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case WTCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case XLMBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case XLMBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case XLMETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case XMRBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case XMRETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case XRPBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case XRPETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case XVGBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case XZCBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case XZCBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case XZCETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case YOYOBNB: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case YOYOBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case YOYOETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0").format(round(value,0)));
                return value.setScale(0,RoundingMode.DOWN).toString();
            case ZECBTC: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case ZECETH: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.000").format(round(value,3)));
                return value.setScale(3,RoundingMode.DOWN).toString();
            case BNBUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00").format(round(value,2)));
                return value.setScale(2,RoundingMode.DOWN).toString();
            case ETHUSDT: //System.out.println("Symbol: " + symbol + " Rounded:" + new DecimalFormat("#0.00000").format(round(value,5)));
                return value.setScale(5,RoundingMode.DOWN).toString();
            default: return "0.0";
        }
    }

    public static void generateSwitchCaseForRoundingDouble(Product product){
        for(Detail detail : product.getData()){
            String minTrade = detail.getMinTrade();
            minTrade = minTrade.split("(?<=1)")[0];
            String minTradeFormat = minTrade.replaceAll("\\d", "0");
            int integerPlaces = minTrade.indexOf('.');
            int decimalPlaces = minTrade.length() - integerPlaces - 1;

            System.out.println("case "
                    + detail.getSymbol()
                    + ": //System.out.println(\"Symbol: \" + symbol + \" Rounded:\" + new DecimalFormat(\"#"
                    + minTradeFormat
                    + "\").format(round(value,"
                    + decimalPlaces + ")));"
            );
            System.out.println("    return new DecimalFormat(\"#"
                    + minTradeFormat
                    + "\").format(round(value,"
                    + decimalPlaces + "));"
            );
        }
    }

    public static void generateSwitchCaseForRoundingBigDecimal(Product product){
        for(Detail detail : product.getData()){
            String minTrade = detail.getMinTrade();
            BigDecimal minTradeB = new BigDecimal(minTrade);
            minTrade = minTrade.split("(?<=1)")[0];
            String minTradeFormat = minTrade.replaceAll("\\d", "0");
            int integerPlaces = minTrade.indexOf('.');
            int decimalPlaces = minTradeB.compareTo(new BigDecimal("1"))==0 ? 0 : minTrade.length() - integerPlaces - 1;

            System.out.println("case "
                    + detail.getSymbol()
                    + ": //System.out.println(\"Symbol: \" + symbol + \" Rounded:\" + new DecimalFormat(\"#"
                    + minTradeFormat
                    + "\").format(round(value,"
                    + decimalPlaces + ")));"
            );

            System.out.println("    return value.setScale("
                    +decimalPlaces
                    +",RoundingMode.DOWN).toString();");

            //return value.setScale(decimalPlaces,BigDecimal.ROUND_HALF_EVEN).toString();

            //new BigDecimal("1").setScale(decimalPlaces,BigDecimal.ROUND_HALF_EVEN);

        }
    }

}