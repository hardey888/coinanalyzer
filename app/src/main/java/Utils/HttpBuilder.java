package Utils;

import android.annotation.SuppressLint;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import apis.BinanceApi_Trade;
import models.Global.Globals;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by hardeylim on 26/01/2018.
 */

public class HttpBuilder {
    @SuppressLint("LongLogTag")
    public BinanceApi_Trade api() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);



        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.addInterceptor(chain -> {
            Request request = chain.request();
            String url = String.valueOf(request.url());

            String[] queryParams = url.split("\\?");

            if(queryParams.length > 1){
                url = url+"&signature="+Utils.getSignature(queryParams[1]);
            }

            return chain.proceed(
                    request.newBuilder()
                            .url(url)
                            .addHeader("X-MBX-APIKEY", Globals.getApiKey())
                            .addHeader("Content-Type", "application/json;charset=UTF-8")
                            .addHeader("Accept", "application/json")
                            .build()
            );

        }).addInterceptor(logging);

        return new Retrofit.Builder()
                .baseUrl("https://api.binance.com/api/")
                .client(builder.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(BinanceApi_Trade.class);
    }
}
