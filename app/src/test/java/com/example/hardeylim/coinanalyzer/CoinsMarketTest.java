package com.example.hardeylim.coinanalyzer;

import android.content.Context;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import Dao.CoinsPhDao;
import apis.CoinsPhApi;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import models.Coinsph.MarketDto;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hardeylim on 05/01/2018.
 */

public class CoinsMarketTest {

    private CoinsPhApi api;


    @Before
    public void setUp(){
        setUpApi();
        //setUpDatabase();
    }

    public void setUpApi() {
        api =
                new Retrofit.Builder()
                        .baseUrl("https://quote.coins.ph/v1/markets/")
                        .client(new OkHttpClient())
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
                        .create(CoinsPhApi.class);


    }

    @Test
    public void loadMarkets() throws IOException {
        Gson gson = new Gson();

        Single.zip(api.peso(),
                api.usd(),
                (pesoMarket, usdMarket) -> Arrays.asList(pesoMarket, usdMarket))
                .subscribe(
                        result -> System.out.println(gson.toJson(result))
                );

        api.peso().subscribe(new Consumer<MarketDto>() {
            @Override
            public void accept(MarketDto marketDto) throws Exception {
                //something
            }
        });
    }
}