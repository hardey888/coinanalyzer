package com.example.hardeylim.coinanalyzer;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import Utils.HttpBuilder;
import Utils.Utils;
import apis.BinanceApi_Trade;
import apis.BinanceApi_v1;
import apis.BinanceApi_v3;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.functions.Consumer;
import models.Binance.Action;
import models.Binance.BookTicker;
import models.Binance.ExecutionPath;
import models.Binance.FullResponse;
import models.Binance.Product;
import models.Binance.Symbol;
import models.Binance.Trade;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static models.Binance.Action.BUY;
import static models.Binance.Action.SELL;
import static models.Binance.Symbol.ETHBTC;
import static models.Binance.Symbol.LTCBTC;
import static models.Binance.Symbol.LTCETH;
import static models.Binance.Symbol.NEOBTC;
import static models.Binance.Symbol.NEOETH;

/**
 * Created by Hardey on 10/02/2018.
 */

public class TriangleArbitrageTest {

    private BinanceApi_v1 api_v1;
    private BinanceApi_v3 api_v3;
    private BinanceApi_Trade api_trade;
    private String v1_URL = "https://api.binance.com/api/v1/";
    private String v3_URL = "https://api.binance.com/api/v3/";
    private List<ExecutionPath> executionPaths = new ArrayList<>();
    private ExecutionPath highestNetGainExecPath = new ExecutionPath();
    private Gson gson = new Gson();
    private Product product = new Product();
    private final BigDecimal STARTING_AMOUNT = new BigDecimal("0.01"); // BTC
    private BigDecimal divisibleInitialValue;
    private BigDecimal endingValue;
    private final String INITIAL_AMOUNT = "0.01";
    private final String INITIAL_COIN = "BTC";
    private String coinAfterExecution = "BTC"; // This is the Base currency to start with.
    private String trailingBalance = "0.0"; //BTC
    private String buySellQuantity = "0.0";

    @Before
    public void setUp(){
        api_v1 =   new Retrofit.Builder()
                .baseUrl(v1_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(BinanceApi_v1.class);

        api_v3 =   new Retrofit.Builder()
                .baseUrl(v3_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(BinanceApi_v3.class);
        api_trade = new HttpBuilder().api();
        getProduct();
        setupExecutionPaths();
    }

    @Test
    public void dodonWave(){
        api_v3.getAllBookTickers()
                .repeat()
                .retry()
                .subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                executionPaths = Utils.populateExecutionPathsSilently(executionPaths, Arrays.asList(bookTickers), STARTING_AMOUNT);
                highestNetGainExecPath = executionPaths.stream().max(Comparator.comparing(ExecutionPath::getNetGain)).get();
                //System.out.println(gson.toJson(highestNetGainExecPath));
                //executeTrades(highestNetGainExecPath);
                if(highestNetGainExecPath.getNetGainPercentage().compareTo(new BigDecimal("0.02")) == 1){
                    System.out.println(gson.toJson(highestNetGainExecPath));
                    executeTrades(highestNetGainExecPath);
                }
            }
        });
    }

    private void executeTrades(ExecutionPath executionPath){
        BigDecimal STARTING_AMOUNT = executionPath.getStartingValue();

        String buySellQuantity = "0.0";
        Trade firstTrade = executionPath.getTrades().getFirst();
        Trade secondTrade = executionPath.getTrades().get(1);
        Trade lastTrade = executionPath.getTrades().getLast();

        this.trailingBalance = executionPath.getStartingValue().toString();
        buySellQuantity = Utils.roundBasedOnPair(firstTrade.getSymbol(), STARTING_AMOUNT.divide(firstTrade.getRate(),8, RoundingMode.HALF_DOWN));
        this.trailingBalance = buySellQuantity.toString();
        BigDecimal exactInitialValue = new BigDecimal(buySellQuantity).multiply(firstTrade.getRate());
        this.divisibleInitialValue = exactInitialValue;

        executeFirstBuy(firstTrade.getSymbol().toString(), buySellQuantity, firstTrade.getSymbol());
        switch (secondTrade.getAction()) {
            case BUY:
                buySellQuantity = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).divide(secondTrade.getRate(), 8, RoundingMode.HALF_DOWN));
                trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).divide(secondTrade.getRate(),8, RoundingMode.HALF_DOWN));
                executeSecondBuy(secondTrade.getSymbol().toString(), buySellQuantity, secondTrade.getSymbol());
                trailingBalance = new BigDecimal(trailingBalance).multiply(lastTrade.getRate()).toString();
                executeLastSell(lastTrade.getSymbol().toString(), buySellQuantity, lastTrade.getSymbol());
                break;
            case SELL:
                trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), new BigDecimal(trailingBalance).multiply(secondTrade.getRate()));
                executeSecondSell(secondTrade.getSymbol().toString(), buySellQuantity, secondTrade.getSymbol());
                buySellQuantity = Utils.roundBasedOnPair(lastTrade.getSymbol(),new BigDecimal(trailingBalance));
                trailingBalance = new BigDecimal(trailingBalance).multiply(lastTrade.getRate()).toString();
                executeLastSell(lastTrade.getSymbol().toString(), buySellQuantity, lastTrade.getSymbol());
                break;
        }
        this.endingValue=new BigDecimal(trailingBalance);
        System.out.println("Initial Deducted Balance" + exactInitialValue);

    }

    private void executeFirstBuy(String pair, String quantity, Symbol tradeSymbol){
        api_trade.buy(getTime(),quantity,pair).subscribe(new Consumer<FullResponse>() {
            @Override
            public void accept(FullResponse fullResponse) throws Exception {
                coinAfterExecution = pair.toString().replace(INITIAL_COIN,"");
                System.out.println("Trade Execution BUY " + tradeSymbol + " Quantity: " + quantity + " " + coinAfterExecution);
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
                System.out.println(gson.toJson(fullResponse));
            }
        });
    }

    private void executeSecondBuy(String pair, String quantity, Symbol tradeSymbol){
        api_trade.buy(getTime(),quantity,pair).subscribe(new Consumer<FullResponse>() {
            @Override
            public void accept(FullResponse fullResponse) throws Exception {
                coinAfterExecution = pair.replace(coinAfterExecution,"");
                System.out.println("Trade Execution BUY " + tradeSymbol + " Quantity: " + quantity + " " + coinAfterExecution);
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
                System.out.println(gson.toJson(fullResponse));
            }
        });
    }

    private void executeSecondSell(String pair, String quantity, Symbol tradeSymbol){
        api_trade.sell(getTime(),quantity, pair).subscribe(new Consumer<FullResponse>() {
            @Override
            public void accept(FullResponse fullResponse) throws Exception {
                System.out.println("Trade Execution SELL " + tradeSymbol + " Quantity: " + quantity + " " + coinAfterExecution);
                coinAfterExecution = pair.replace(coinAfterExecution,"");
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
                System.out.println(gson.toJson(fullResponse));
            }
        });
    }

    private void executeLastSell(String pair, String quantity, Symbol tradeSymbol){
        api_trade.sell(getTime(), quantity, pair).subscribe(new Consumer<FullResponse>() {
            @Override
            public void accept(FullResponse fullResponse) throws Exception {
                System.out.println("Trade Execution SELL " + tradeSymbol + " Quantity: " + quantity + " " + coinAfterExecution);
                coinAfterExecution = pair.replace(coinAfterExecution,"");
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
                System.out.println(gson.toJson(fullResponse));
            }
        });
    }

    private void mockFirstBuy(String firstPair, String quantity) {
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                BookTicker bookticker = Arrays.stream(bookTickers).filter(bookTicker -> bookTicker.getSymbol().equals(firstPair)).findFirst().get();
                coinAfterExecution = firstPair.toString().replace(INITIAL_COIN,"");
                System.out.println("Trade Execution BUY " + bookticker.getSymbol() + " Quantity: " + quantity + " " + coinAfterExecution);
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
            }
        });
    }

    private void mockSecondBuy(String secondPair, String quantity){
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                BookTicker bookticker = Arrays.stream(bookTickers).filter(bookTicker -> bookTicker.getSymbol().equals(secondPair)).findFirst().get();
                coinAfterExecution = secondPair.replace(coinAfterExecution,"");
                System.out.println("Trade Execution BUY " + bookticker.getSymbol() + " Quantity: " + quantity + " " + coinAfterExecution);
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
            }
        });
    }

    private void mockSecondSell(String secondPair, String quantity){
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                BookTicker bookticker = Arrays.stream(bookTickers).filter(bookTicker -> bookTicker.getSymbol().equals(secondPair)).findFirst().get();
                System.out.println("Trade Execution SELL " + bookticker.getSymbol() + " Quantity: " + quantity + " " + coinAfterExecution);
                coinAfterExecution = secondPair.replace(coinAfterExecution,"");
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
            }
        });
    }

    private void mockLastSell(String lastPair, String quantity){
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                BookTicker bookticker = Arrays.stream(bookTickers).filter(bookTicker -> bookTicker.getSymbol().equals(lastPair)).findFirst().get();
                System.out.println("Trade Execution SELL " + bookticker.getSymbol() + " Quantity: " + quantity + " " + coinAfterExecution);
                coinAfterExecution = lastPair.replace(coinAfterExecution,"");
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
            }
        });
    }




    @Test
    public void testBTCTriangles(){
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                executionPaths = Utils.findBTCTriangles(Arrays.asList(bookTickers));
                System.out.println("Json: " + gson.toJson(executionPaths));
            }
        });
    }

    private void setupExecutionPaths(){
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                executionPaths = Utils.findBTCTriangles(Arrays.asList(bookTickers));
            }
        });
    }

    @Test
    public void testCompute(){
        ExecutionPath executionPath = new ExecutionPath();
        Trade trade1 = new Trade(LTCBTC, new BigDecimal(3), BUY);
        Trade trade2 = new Trade(LTCETH, new BigDecimal(2), BUY);
        Trade trade3 = new Trade(ETHBTC, new BigDecimal(3), SELL);

        executionPath.setStartingValue(new BigDecimal(36));
        executionPath.getTrades().add(trade1);
        executionPath.getTrades().add(trade2);
        executionPath.getTrades().add(trade3);

        executionPath.computeComplexEndingValue();



    }

    @Test
    public void testPopulateExecutionPaths(){
        populateExecutionPaths();
    }

    @Test
    public void testGetProduct(){
        getProduct();
        getRounding();
    }

    @Test
    public void testGetRounding(){
        getRounding();
    }


    private void populateExecutionPaths(){
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                executionPaths = Utils.populateExecutionPaths(executionPaths, Arrays.asList(bookTickers), STARTING_AMOUNT);
                highestNetGainExecPath = executionPaths.stream().max(Comparator.comparing(ExecutionPath::getNetGain)).get();
                System.out.println(gson.toJson(highestNetGainExecPath));
            }
        });
    }
    private void getProduct(){
        api_trade.getProduct().subscribe(new Consumer<Product>() {
            @Override
            public void accept(Product result) throws Exception {
                product = result;
                System.out.println(gson.toJson(product));
            }
        });
    }
    private void getRounding(){
        Utils.generateSwitchCaseForRoundingBigDecimal(product);
    }
    private long getTime(){
        return System.currentTimeMillis()-1000;
    }




//    @Test
//    public void mockExecuteTrades(){
//        populateExecutionPaths();
//        System.out.println("Trade Action 2: " + (highestNetGainExecPath.getTrades().get(1).getAction()));
//        if(highestNetGainExecPath.getTrades().get(1).getAction().equals(Action.BUY)) {
//            Trade firstTrade = highestNetGainExecPath.getTrades().getFirst();
//            Trade secondTrade = highestNetGainExecPath.getTrades().get(1);
//            Trade lastTrade = highestNetGainExecPath.getTrades().getLast();
//            trailingBalance = INITIAL_AMOUNT;
//            trailingBalance = Utils.roundBasedOnPair(firstTrade.getSymbol(), STARTING_AMOUNT / firstTrade.getRate());
//            buySellQuantity = Utils.roundBasedOnPair(firstTrade.getSymbol(), STARTING_AMOUNT / firstTrade.getRate());
//            double exactInitialValue = Double.parseDouble(buySellQuantity) * firstTrade.getRate();
//            mockFirstBuy(firstTrade.getSymbol().toString(), buySellQuantity);
//            switch (secondTrade.getAction()) {
//                case BUY:
//                    buySellQuantity = Utils.roundBasedOnPair(secondTrade.getSymbol(), Double.parseDouble(trailingBalance) / secondTrade.getRate());
//                    trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), Double.parseDouble(trailingBalance) / secondTrade.getRate());
//                    mockSecondBuy(secondTrade.getSymbol().toString(), buySellQuantity);
//                    trailingBalance = Double.toString(Double.parseDouble(trailingBalance) * lastTrade.getRate());
//                    mockLastSell(lastTrade.getSymbol().toString(), buySellQuantity);
//                    System.out.println("Initial Deducted Balance" + exactInitialValue);
//                    break;
//                case SELL:
//                    trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), Double.parseDouble(trailingBalance) * secondTrade.getRate());
//                    mockSecondSell(secondTrade.getSymbol().toString(), buySellQuantity);
//                    buySellQuantity = Utils.roundBasedOnPair(lastTrade.getSymbol(), Double.parseDouble(buySellQuantity) * lastTrade.getRate());
//                    trailingBalance = Utils.roundBasedOnPair(lastTrade.getSymbol(), Double.parseDouble(trailingBalance) * lastTrade.getRate());
//                    mockLastSell(lastTrade.getSymbol().toString(), trailingBalance);
//                    break;
//            }
//        }
//    }

    //    @Test
//    private void executeTrades(){
//        if(highestNetGainExecPath.getTrades().get(1).getAction().equals(Action.BUY)) {
//            Trade firstTrade = highestNetGainExecPath.getTrades().getFirst();
//            Trade secondTrade = highestNetGainExecPath.getTrades().get(1);
//            Trade lastTrade = highestNetGainExecPath.getTrades().getLast();
//            trailingBalance = INITIAL_AMOUNT;
//            trailingBalance = Utils.roundBasedOnPair(firstTrade.getSymbol(), STARTING_AMOUNT / firstTrade.getRate());
//            buySellQuantity = Utils.roundBasedOnPair(firstTrade.getSymbol(), STARTING_AMOUNT / firstTrade.getRate());
//            executeFirstBuy(firstTrade.getSymbol().toString(), buySellQuantity);
//            switch (secondTrade.getAction()) {
//                case BUY:
//                    buySellQuantity = Utils.roundBasedOnPair(secondTrade.getSymbol(), Double.parseDouble(trailingBalance) / secondTrade.getRate());
//                    trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), Double.parseDouble(trailingBalance) / secondTrade.getRate());
//                    mockSecondBuy(secondTrade.getSymbol().toString(), buySellQuantity);
//                    trailingBalance = Double.toString(Double.parseDouble(trailingBalance) * lastTrade.getRate());
//                    mockLastSell(lastTrade.getSymbol().toString(), buySellQuantity);
//                    break;
//                case SELL:
//                    trailingBalance = Utils.roundBasedOnPair(secondTrade.getSymbol(), Double.parseDouble(trailingBalance) * secondTrade.getRate());
//                    mockSecondSell(secondTrade.getSymbol().toString(), buySellQuantity);
//                    buySellQuantity = Utils.roundBasedOnPair(lastTrade.getSymbol(), Double.parseDouble(buySellQuantity) * lastTrade.getRate());
//                    trailingBalance = Utils.roundBasedOnPair(lastTrade.getSymbol(), Double.parseDouble(trailingBalance) * lastTrade.getRate());
//                    mockLastSell(lastTrade.getSymbol().toString(), trailingBalance);
//                    break;
//            }
//        }
//    }
}
