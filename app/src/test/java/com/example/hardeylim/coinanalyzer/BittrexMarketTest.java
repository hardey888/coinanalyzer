package com.example.hardeylim.coinanalyzer;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import apis.BittrexApi;
import apis.CoinsPhApi;
import io.reactivex.functions.Consumer;
import models.Bittrex.AllMarkets;
import models.Bittrex.Currency;
import models.Bittrex.Market;
import models.Bittrex.OrderBook;
import models.Coinsph.MarketDto;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hardeylim on 08/01/2018.
 */

public class BittrexMarketTest {

    private BittrexApi api;
    private List<String> marketNames = new ArrayList<>();

    @Before
    public void setUp(){
        api =           new Retrofit.Builder()
                        .baseUrl("https://bittrex.com/api/v1.1/public/")
                        .client(new OkHttpClient())
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
                        .create(BittrexApi.class);
    }

    @Test
    public void getMarkets(){
        List<Market> markets = new ArrayList<>();
        Gson gson = new Gson();
        api.getMarkets().subscribe(results -> markets.addAll(results.getResult()));
        setMarketNames(markets);
        //System.out.println(gson.toJson(marketNames));
    }


    @Test
    public void getCurrencies(){
        List<Currency> currencies = new ArrayList<>();
        Gson gson = new Gson();
        api.getCurrencies().subscribe(results -> currencies.addAll(results.getResult()));
        System.out.println(gson.toJson(currencies));
    }

    private void setMarketNames(List<Market> markets){
        marketNames = markets.stream().map(Market::getMarketName).collect(Collectors.toList());
    }

    @Test
    public void getOrderBookByPair(){
        String pair = "BTC-LTC";
        String type = "both";
        Gson gson = new Gson();
        //api.getOrderBook(pair, type).subscribe(
          //      Orderbooks -> System.out.println(gson.toJson(
            //            Orderbooks.getResult().getBuy().stream().mapToDouble(OrderBook.Order::getQuantity).sum())));

        api.getOrderBook(pair, type).subscribe(
                OrderBooks -> System.out.println("Buy Total: " + gson.toJson(
                    OrderBooks.getResult().getBuyTotal()
                )
            )
        );

        api.getOrderBook(pair, type).subscribe(
                OrderBooks -> System.out.println("Sell Total: " + gson.toJson(
                        OrderBooks.getResult().getSellTotal()
                        )
                )
        );

    }

























}
