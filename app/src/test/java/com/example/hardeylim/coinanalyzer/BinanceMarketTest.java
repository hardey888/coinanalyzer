package com.example.hardeylim.coinanalyzer;

import android.widget.Switch;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import Utils.HttpBuilder;
import Utils.Utils;
import apis.BinanceApi_Trade;
import apis.BinanceApi_v1;
import apis.BinanceApi_v3;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import models.Binance.BookTicker;
import models.Binance.CandleStick;
import models.Binance.ExecutionPath;
import models.Binance.FullResponse;
import models.Binance.Interval;
import models.Binance.KLines;
import models.Binance.Symbol;
import models.Binance.Ticker;
import models.Binance.Time;
import models.Binance.TransactionLog;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Hardey on 13/01/2018.
 */

public class BinanceMarketTest {

    private BinanceApi_v1 api_v1;
    private BinanceApi_v3 api_v3;
    private BinanceApi_Trade api_trade;
    private String v1_URL = "https://api.binance.com/api/v1/";
    private String v3_URL = "https://api.binance.com/api/v3/";
    private Gson gson = new Gson();
    private DecimalFormat percentageFormat = new DecimalFormat("##.##%");
    private TransactionLog transactionLog = new TransactionLog();
    private Ticker maxUSDT = new Ticker();
    private Ticker minUSDT = new Ticker();
    private static final Double initialBalance = 120.0; //USDT
    private static final String initialCoin = "USDT";
    private Double endingBalance = 0.0; //USDT
    private String trailingBalance = "0.0"; //USDT
    private String coinAfterExecution = "USDT"; // This is the Base currency to start with.
    private List<String> neoPairs = new ArrayList<String>() {{add("NEOUSDT");
                                                                add("NEOBTC");
                                                                add("NEOBNB");
                                                                add("NEOETH");
                                                                add("BTCUSDT");
                                                                add("BNBUSDT");
                                                                add("ETHUSDT");}};
    private List<String> bccPairs = new ArrayList<String>() {{add("BCCUSDT");
                                                                add("BCCBTC");
                                                                add("BCCBNB");
                                                                add("BCCETH");
                                                                add("BTCUSDT");
                                                                add("BNBUSDT");
                                                                add("ETHUSDT");}};
    @Before
    public void setUp(){
        api_v1 =   new Retrofit.Builder()
                .baseUrl(v1_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(BinanceApi_v1.class);

        api_v3 =   new Retrofit.Builder()
                .baseUrl(v3_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(BinanceApi_v3.class);

        api_trade = new HttpBuilder().api();
    }

    @Test
    public void testBuySell(){
        Gson gson = new Gson();
        api_trade.buyTest(getTime(),"95.52000","ETHUSDT")
                .delay(1, TimeUnit.SECONDS)
                .subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                System.out.println("Success");
                System.out.println(s);
            }
        });
        api_trade.buyTest(getTime(),"95.52000","ETHUSDT")
                .delay(1, TimeUnit.SECONDS)
                .subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                System.out.println("Success");
                System.out.println(s);
            }
        });
        api_trade.sellTest(getTime(),"95.52000","ETHUSDT")
                .delay(1, TimeUnit.SECONDS)
                .subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                System.out.println("Success");
                System.out.println(s);
            }
        });
        api_trade.sellTest(getTime(),"95.52000","ETHUSDT")
                    //.repeat()
                    //.retry()
                    .delay(1, TimeUnit.SECONDS)
                    .subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                System.out.println("Success");
                System.out.println(s);
            }
        });

    }

    @Test
    public  void testServerTime(){
        api_trade.buyTest(System.currentTimeMillis(),"95.52000","ETHUSDT")
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        System.out.println("Success");
                        System.out.println(s);
                    }
                });
    }

    @Test
    public void compareServerTime(){
        api_v1.getServerTime()
                .retry()
                .delay(1, TimeUnit.SECONDS)
                .repeat()
                .delay(new Long(1), TimeUnit.SECONDS)
                .subscribe(new Consumer<Time>() {
            @Override
            public void accept(Time time) throws Exception {
                System.out.println("Success");
                if(System.currentTimeMillis()==time.getServerTime()){
                    System.out.println("Times are identical");
                }else{
                    System.out.println("Server Time : "+ time.getServerTime()+" System Time: " + System.currentTimeMillis());
                }
            }
        });
    }

    @Test
    public void testBTCTriangles(){
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                List<ExecutionPath> executionPaths = new ArrayList<>();
                executionPaths = Utils.findBTCTriangles(Arrays.asList(bookTickers));
            }
        });
    }

    @Test
    public void testForNewPairs(){
        api_v3.getAllTickers().subscribe(new Consumer<Ticker[]>() {
            @Override
            public void accept(Ticker[] tickers) throws Exception {
                List<String> newTickers = Utils.checkForNewTickers(Arrays.asList(tickers));
                System.out.println(gson.toJson(newTickers));
            }
        });

        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] bookTickers) throws Exception {
                List<String> newTickers = Utils.checkForNewBookTickers(Arrays.asList(bookTickers));
                System.out.println(gson.toJson(newTickers));            }
        });
    }

    @Test
    public void watchNeoPairs(){
        NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        DecimalFormat df = new DecimalFormat("##.##%");
        Gson gson = new Gson();

        api_v3.getAllTickers()//.retry()
                //.delay(1, TimeUnit.SECONDS)
                //.repeat()
                //.observeOn(AndroidSchedulers.mainThread())
                //.subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Ticker[]>() {
                    @Override
                    public void accept(Ticker[] results) throws Exception {
                        List<Ticker> tickers = Arrays.asList(results);
                        List<Ticker> neoList = tickers.stream().filter(ticker -> neoPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                    //List<Ticker> ltcList = tickers.stream().filter(ticker -> ltcPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                        //List<Ticker> bccList = tickers.stream().filter(ticker -> bccPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                        processNeoTickers(tickers);
                        //System.out.println(gson.toJson(neoList));
                    }
                });
    }

    @Test
    public void testSellAPI(){
        api_trade.sell(System.currentTimeMillis(),"99.94","BNBUSDT").subscribe();
    }

    @Test
    public void getAllNeoBookTickers() throws InterruptedException {
        Gson gson = new Gson();
        api_v3.getAllBookTickers()//.retry()
                                  //.repeat()
                                  //.subscribeOn(Schedulers.io())
                .retry()
                .delay(1, TimeUnit.SECONDS)
                .repeat()
                //.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                                  .subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] results) throws Exception {
                List<BookTicker> bookTickers = Arrays.asList(results);
                List<BookTicker> neoList = bookTickers.stream().filter(ticker -> neoPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                System.out.println(gson.toJson(neoList));
                //processNeoBookTickers(bookTickers);

            }
        });

        Thread.sleep(1500);
    }

    @Test
    public void getAllBCCBookTickers(){
        Gson gson = new Gson();
        api_v3.getAllBookTickers().subscribe(new Consumer<BookTicker[]>() {
            @Override
            public void accept(BookTicker[] results) throws Exception {
                List<BookTicker> bookTickers = Arrays.asList(results);
                List<BookTicker> bccList = bookTickers.stream().filter(ticker -> bccPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                System.out.println(gson.toJson(bccList));
                //processBCCBookTickers(bookTickers);
            }
        });
    }

    private void processNeoTickers(List<Ticker> tickers){
        Ticker NEOUSDTTicker = tickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOUSDT")).findFirst().get();

        Ticker NEOBTC = tickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOBTC")).findFirst().get();
        Ticker NEOBNB = tickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOBNB")).findFirst().get();
        Ticker NEOETH = tickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOETH")).findFirst().get();
        Ticker BTCUSDT = tickers.stream().filter(ticker -> ticker.getSymbol().equals("BTCUSDT")).findFirst().get();
        Ticker BNBUSDT = tickers.stream().filter(ticker -> ticker.getSymbol().equals("BNBUSDT")).findFirst().get();
        Ticker ETHUSDT = tickers.stream().filter(ticker -> ticker.getSymbol().equals("ETHUSDT")).findFirst().get();

        Double NEOUSD = NEOUSDTTicker.getPrice();
        Double NEOBTCUSDT = NEOBTC.getPrice() * BTCUSDT.getPrice();
        Double NEOETHUSDT = NEOETH.getPrice() * ETHUSDT.getPrice();
        Double NEOBNBUSDT = NEOBNB.getPrice() * BNBUSDT.getPrice();
        Ticker NEOBTCUSDTTicker = new Ticker("NEOBTCUSDT", NEOBTCUSDT);
        Ticker NEOETHUSDTTicker = new Ticker("NEOETHUSDT", NEOETHUSDT);
        Ticker NEOBNBUSDTTicker = new Ticker("NEOBNBUSDT", NEOBNBUSDT);

        List<Ticker> tickersInUSDT = new ArrayList<Ticker>();
        tickersInUSDT.add(NEOUSDTTicker);
        tickersInUSDT.add(NEOBTCUSDTTicker);
        tickersInUSDT.add(NEOETHUSDTTicker);
        tickersInUSDT.add(NEOBNBUSDTTicker);

        maxUSDT = tickersInUSDT.stream().max(Comparator.comparing(Ticker::getPrice)).get();
        minUSDT = tickersInUSDT.stream().min(Comparator.comparing(Ticker::getPrice)).get();
        Double netGain = maxUSDT.getPrice() - minUSDT.getPrice();
        Double percentageGain = netGain/minUSDT.getPrice();
        Date dateTime = new Date();
        dateTime.setTime(System.currentTimeMillis());

        if(netGain > transactionLog.getNetGain()){
            transactionLog.store(maxUSDT, minUSDT, netGain, percentageGain, System.currentTimeMillis(), dateTime);
        }

        Double NETNEOBTCUSDT = (NEOBTCUSDT - NEOUSD) / NEOUSD;
        Double NETNEOETHUSDT = (NEOETHUSDT - NEOUSD) / NEOUSD;
        Double NETNEOBNBUSDT = (NEOBNBUSDT - NEOUSD) / NEOUSD;

        System.out.println("NEO ->        USDT: " + String.format("%.10f", NEOUSDTTicker.getPrice()));
        System.out.println("NEO -> BTC -> USDT: " + String.format("%.10f", NEOBTC.getPrice() * BTCUSDT.getPrice()) + " NET: " + String.format("%.10f", NEOBTCUSDT - NEOUSD) + " Percentage: " + percentageFormat.format(NETNEOBTCUSDT));
        System.out.println("NEO -> ETH -> USDT: " + String.format("%.10f", NEOETH.getPrice() * ETHUSDT.getPrice()) + " NET: " + String.format("%.10f", NEOETHUSDT - NEOUSD) + " Percentage: " + percentageFormat.format(NETNEOETHUSDT));
        System.out.println("NEO -> BNB -> USDT: " + String.format("%.10f", NEOBNB.getPrice() * BNBUSDT.getPrice()) + " NET: " + String.format("%.10f", NEOBNBUSDT - NEOUSD) + " Percentage: " + percentageFormat.format(NETNEOBNBUSDT));

        System.out.println("Minimum: " + minUSDT.getSymbol() + " " + minUSDT.getPrice());
        System.out.println("Maximum: " + maxUSDT.getSymbol() + " " + maxUSDT.getPrice());
        System.out.println("Net Gain : " + transactionLog.getNetGain() + " "  + percentageFormat.format(transactionLog.getPercentageGain()));

        //activity.showNeoUSDT(NEOUSDTTicker);
        //activity.showNeoBtcUSDT(NEOBTCUSDTTicker);
        //activity.showNeoEthUSDT(NEOETHUSDTTicker);
        //activity.showNeoBnbUSDT(NEOBNBUSDTTicker);

        //activity.showMaxUSDT(maxUSDT);
        //activity.showMinUSDT(minUSDT);
        //activity.showNeoNetGain(maxUSDT.getPrice()-minUSDT.getPrice());
        //activity.showLog(transactionLog);

        testTradeExecution(maxUSDT,minUSDT);
        double actualGain = endingBalance-initialBalance;
        double actualPercentageGain = actualGain/initialBalance;
        System.out.println("Expected Net Gain : " + transactionLog.getNetGain() + " "  + percentageFormat.format(transactionLog.getPercentageGain()));
        System.out.println("Actual   Net Gain : " + actualGain + " " +  percentageFormat.format(actualPercentageGain));
    }

//    private void processBCCBookTickers(List<BookTicker> bookTickers){
//
//        List<Ticker> buySideTickers = new ArrayList<Ticker>();
//        List<Ticker> sellSideTickers = new ArrayList<Ticker>();
//        List<BookTicker> filteredBookTickers = new ArrayList<BookTicker>();
//        BookTicker BCCUSDTBookTicker = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals("BCCUSDT")).findFirst().get();
//
//        BookTicker BCCBTC = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("BCCBTC")).findFirst().get();
//        BookTicker BCCBNB = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("BCCBNB")).findFirst().get();
//        BookTicker BCCETH = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("BCCETH")).findFirst().get();
//        BookTicker BTCUSDT = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("BTCUSDT")).findFirst().get();
//        BookTicker BNBUSDT = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("BNBUSDT")).findFirst().get();
//        BookTicker ETHUSDT = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("ETHUSDT")).findFirst().get();
//
//        filteredBookTickers.add(BCCUSDTBookTicker);
//        filteredBookTickers.add(BCCBTC);
//        filteredBookTickers.add(BCCBNB);
//        filteredBookTickers.add(BCCETH);
//        filteredBookTickers.add(BTCUSDT);
//        filteredBookTickers.add(BNBUSDT);
//        filteredBookTickers.add(ETHUSDT);
//
//        List<Ticker> askTickers = getAllAsks(filteredBookTickers);
//        List<Ticker> bidTickers = getAllBids(filteredBookTickers);
//
//        //Buy Side - Lowest that are willing to Sell
//        Double BCCUSDT = BCCUSDTBookTicker.getAskPrice();
//        Double BCCBTCUSDT = BCCBTC.getAskPrice() * BTCUSDT.getAskPrice();
//        Double BCCETHUSDT = BCCETH.getAskPrice() * ETHUSDT.getAskPrice();
//        Double BCCBNBUSDT = BCCBNB.getAskPrice() * BNBUSDT.getAskPrice();
//        Ticker BCCUSDTTicker = new Ticker("BCCUSDT", BCCUSDT);
//        Ticker BCCBTCUSDTTicker = new Ticker("BCCBTCUSDT", BCCBTCUSDT);
//        Ticker BCCETHUSDTTicker = new Ticker("BCCETHUSDT", BCCETHUSDT);
//        Ticker BCCBNBUSDTTicker = new Ticker("BCCBNBUSDT", BCCBNBUSDT);
//
//        buySideTickers.add(BCCUSDTTicker);
//        buySideTickers.add(BCCBTCUSDTTicker);
//        buySideTickers.add(BCCETHUSDTTicker);
//        buySideTickers.add(BCCBNBUSDTTicker);
//
//        //Sell Side - Highest that are willing to Buy
//        BCCUSDT = BCCUSDTBookTicker.getBidPrice();
//        BCCBTCUSDT = BCCBTC.getBidPrice() * BTCUSDT.getBidPrice();
//        BCCETHUSDT = BCCETH.getBidPrice() * ETHUSDT.getBidPrice();
//        BCCBNBUSDT = BCCBNB.getBidPrice() * BNBUSDT.getBidPrice();
//        BCCUSDTTicker.setTicker("BCCUSDT", BCCUSDT);
//        BCCBTCUSDTTicker.setTicker("BCCBTCUSDT", BCCBTCUSDT);
//        BCCETHUSDTTicker.setTicker("BCCETHUSDT", BCCETHUSDT);
//        BCCBNBUSDTTicker.setTicker("BCCBNBUSDT", BCCBNBUSDT);
//
//        sellSideTickers.add(BCCUSDTTicker);
//        sellSideTickers.add(BCCBTCUSDTTicker);
//        sellSideTickers.add(BCCETHUSDTTicker);
//        sellSideTickers.add(BCCBNBUSDTTicker);
//
//        minUSDT = buySideTickers.stream().min(Comparator.comparing(Ticker::getPrice)).get();
//        maxUSDT = sellSideTickers.stream().max(Comparator.comparing(Ticker::getPrice)).get();
//
//        Double netGain = maxUSDT.getPrice() - minUSDT.getPrice();
//        Double percentageGain = netGain/minUSDT.getPrice();
//        Date dateTime = new Date();
//        dateTime.setTime(System.currentTimeMillis());
//
//        if(netGain > transactionLog.getNetGain()){
//            transactionLog.store(maxUSDT, minUSDT, netGain, percentageGain, System.currentTimeMillis(), dateTime);
//        }
//
//        System.out.println("Minimum: " + minUSDT.getSymbol() + " " + minUSDT.getPrice());
//        System.out.println("Maximum: " + maxUSDT.getSymbol() + " " + maxUSDT.getPrice());
//        System.out.println("Net Gain : " + transactionLog.getNetGain() + " "  + percentageFormat.format(transactionLog.getPercentageGain()));
//
//        //activity.showNeoUSDT(BCCUSDTTicker);
//        //activity.showNeoBtcUSDT(BCCBTCUSDTTicker);
//        //activity.showNeoEthUSDT(BCCETHUSDTTicker);
//        //activity.showNeoBnbUSDT(BCCBNBUSDTTicker);
//
//        //activity.showMaxUSDT(maxUSDT);
//        //activity.showMinUSDT(minUSDT);
//        //activity.showNeoNetGain(maxUSDT.getPrice()-minUSDT.getPrice());
//        //activity.showLog(transactionLog);
//
//        actualTradeExecution(maxUSDT,minUSDT,bookTickers);
//        double actualGain = Double.parseDouble(trailingBalance)-initialBalance;
//        //double actualGain = endingBalance-initialBalance;
//        double actualPercentageGain = actualGain/initialBalance;
//        System.out.println("Expected Net Gain : " + transactionLog.getNetGain() + " "  + percentageFormat.format(transactionLog.getPercentageGain()));
//        System.out.println("Actual   Net Gain : " + actualGain + " " +  percentageFormat.format(actualPercentageGain));
//
//    }

//    private void processNeoBookTickers(List<BookTicker> bookTickers){
//
//        List<Ticker> buySideTickers = new ArrayList<Ticker>();
//        List<Ticker> sellSideTickers = new ArrayList<Ticker>();
//        List<BookTicker> filteredBookTickers = new ArrayList<BookTicker>();
//        BookTicker NEOUSDTBookTicker = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals("NEOUSDT")).findFirst().get();
//
//        BookTicker NEOBTC = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("NEOBTC")).findFirst().get();
//        BookTicker NEOBNB = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("NEOBNB")).findFirst().get();
//        BookTicker NEOETH = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("NEOETH")).findFirst().get();
//        BookTicker BTCUSDT = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("BTCUSDT")).findFirst().get();
//        BookTicker BNBUSDT = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("BNBUSDT")).findFirst().get();
//        BookTicker ETHUSDT = bookTickers.stream().filter(bookTicker -> bookTicker.getSymbol().equals("ETHUSDT")).findFirst().get();
//
//        filteredBookTickers.add(NEOUSDTBookTicker);
//        filteredBookTickers.add(NEOBTC);
//        filteredBookTickers.add(NEOBNB);
//        filteredBookTickers.add(NEOETH);
//        filteredBookTickers.add(BTCUSDT);
//        filteredBookTickers.add(BNBUSDT);
//        filteredBookTickers.add(ETHUSDT);
//
//        List<Ticker> askTickers = getAllAsks(filteredBookTickers);
//        List<Ticker> bidTickers = getAllBids(filteredBookTickers);
//
//        //Buy Side - Cheapest that are willing to Sell
//        Double NEOUSDT = NEOUSDTBookTicker.getAskPrice();
//        Double NEOBTCUSDT = NEOBTC.getAskPrice() * BTCUSDT.getAskPrice();
//        Double NEOETHUSDT = NEOETH.getAskPrice() * ETHUSDT.getAskPrice();
//        Double NEOBNBUSDT = NEOBNB.getAskPrice() * BNBUSDT.getAskPrice();
//        Ticker NEOUSDTTicker = new Ticker("NEOUSDT", NEOUSDT);
//        Ticker NEOBTCUSDTTicker = new Ticker("NEOBTCUSDT", NEOBTCUSDT);
//        Ticker NEOETHUSDTTicker = new Ticker("NEOETHUSDT", NEOETHUSDT);
//        Ticker NEOBNBUSDTTicker = new Ticker("NEOBNBUSDT", NEOBNBUSDT);
//
//        buySideTickers.add(NEOUSDTTicker);
//        buySideTickers.add(NEOBTCUSDTTicker);
//        buySideTickers.add(NEOETHUSDTTicker);
//        buySideTickers.add(NEOBNBUSDTTicker);
//
//        //Sell Side - Highest that are willing to Buy
//        NEOUSDT = NEOUSDTBookTicker.getBidPrice();
//        NEOBTCUSDT = NEOBTC.getBidPrice() * BTCUSDT.getBidPrice();
//        NEOETHUSDT = NEOETH.getBidPrice() * ETHUSDT.getBidPrice();
//        NEOBNBUSDT = NEOBNB.getBidPrice() * BNBUSDT.getBidPrice();
//        NEOUSDTTicker.setTicker("NEOUSDT", NEOUSDT);
//        NEOBTCUSDTTicker.setTicker("NEOBTCUSDT", NEOBTCUSDT);
//        NEOETHUSDTTicker.setTicker("NEOETHUSDT", NEOETHUSDT);
//        NEOBNBUSDTTicker.setTicker("NEOBNBUSDT", NEOBNBUSDT);
//
//        sellSideTickers.add(NEOUSDTTicker);
//        sellSideTickers.add(NEOBTCUSDTTicker);
//        sellSideTickers.add(NEOETHUSDTTicker);
//        sellSideTickers.add(NEOBNBUSDTTicker);
//
//        minUSDT = buySideTickers.stream().min(Comparator.comparing(Ticker::getPrice)).get();
//        maxUSDT = sellSideTickers.stream().max(Comparator.comparing(Ticker::getPrice)).get();
//
//        Double netGain = maxUSDT.getPrice() - minUSDT.getPrice();
//        Double percentageGain = netGain/minUSDT.getPrice();
//        Date dateTime = new Date();
//        dateTime.setTime(System.currentTimeMillis());
//
//        if(netGain > transactionLog.getNetGain()){
//            transactionLog.store(maxUSDT, minUSDT, netGain, percentageGain, System.currentTimeMillis(), dateTime);
//        }
//
//        System.out.println("Minimum: " + minUSDT.getSymbol() + " " + minUSDT.getPrice());
//        System.out.println("Maximum: " + maxUSDT.getSymbol() + " " + maxUSDT.getPrice());
//        System.out.println("Net Gain : " + netGain + " "  + percentageFormat.format(percentageGain));
//
//        //activity.showNeoUSDT(NEOUSDTTicker);
//        //activity.showNeoBtcUSDT(NEOBTCUSDTTicker);
//        //activity.showNeoEthUSDT(NEOETHUSDTTicker);
//        //activity.showNeoBnbUSDT(NEOBNBUSDTTicker);
//
//        //activity.showMaxUSDT(maxUSDT);
//        //activity.showMinUSDT(minUSDT);
//        //activity.showNeoNetGain(maxUSDT.getPrice()-minUSDT.getPrice());
//        //activity.showLog(transactionLog);
////        if(percentageGain) {
////            actualTradeExecution(maxUSDT, minUSDT, bookTickers);
////            double actualGain = Double.parseDouble(trailingBalance) - initialBalance;
////            double actualPercentageGain = actualGain / initialBalance;
////            System.out.println("Expected Net Gain : " + transactionLog.getNetGain() + " " + percentageFormat.format(transactionLog.getPercentageGain()));
////            System.out.println("Actual   Net Gain : " + actualGain + " " + percentageFormat.format(actualPercentageGain));
////        }
//        System.out.println(percentageGain);
//
//    }

//    private void mockTradeExecution(Ticker maximum, Ticker minimum, List<BookTicker> bookTickers){
//        trailingBalance = initialBalance.toString();
//        String firstBuyPair = extractFirstOrLastPair(minimum);
//        String lastSellPair = extractFirstOrLastPair(maximum);
//        Double firstPairRate = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals(firstBuyPair)).findFirst().get().getAskPrice();
//        Double lastSellPairRate = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals(lastSellPair)).findFirst().get().getBidPrice();
//        trailingBalance = roundBasedOnPair(firstBuyPair,Double.parseDouble(trailingBalance)/firstPairRate);
//        mockFirstBuy(firstBuyPair, trailingBalance);
//        if(minimum.getSymbol().length()>7) { //If minimum ticker is a complete pair NEOUSDT=7 NEOBNBUSDT=10
//            String additionalBuyPair = removeUSDTFromTicker(minimum);
//            Double additionalBuyPairRate = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals(additionalBuyPair)).findFirst().get().getAskPrice();
//            trailingBalance = roundBasedOnPair(additionalBuyPair,Double.parseDouble(trailingBalance)/additionalBuyPairRate);
//            mockSecondBuy(additionalBuyPair,trailingBalance);
//        }
//        if(maximum.getSymbol().length()>7){
//            String additionalSellPair = removeUSDTFromTicker(maximum);
//            Double additionalSellPairRate = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals(additionalSellPair)).findFirst().get().getBidPrice();
//            System.out.println("Additional Sell Pair     : " + additionalSellPair);
//            System.out.println("Additional Sell Pair Rate: " + additionalSellPairRate);
//            System.out.println("Double.parseDouble(trailingBalance) = " + Double.parseDouble(trailingBalance) + " x " + "AdditionalSellPairRate");
//            trailingBalance = roundBasedOnPair(additionalSellPair,Double.parseDouble(trailingBalance)*additionalSellPairRate);
//            mockAdditionalSell(additionalSellPair,trailingBalance);
//        }
//        trailingBalance=roundBasedOnPair(lastSellPair,Double.parseDouble(trailingBalance)*lastSellPairRate);
//        mockLastSell(lastSellPair,trailingBalance);
//    }


//    private void actualTradeExecution(Ticker maximum, Ticker minimum, List<BookTicker> bookTickers){
//        trailingBalance = initialBalance.toString();
//        String firstBuyPair = extractFirstOrLastPair(minimum);
//        String lastSellPair = extractFirstOrLastPair(maximum);
//        Double firstPairRate = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals(firstBuyPair)).findFirst().get().getAskPrice();
//        Double lastSellPairRate = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals(lastSellPair)).findFirst().get().getBidPrice();
//        trailingBalance = roundBasedOnPair(firstBuyPair,Double.parseDouble(trailingBalance)/firstPairRate);
//        executeFirstBuy(firstBuyPair, trailingBalance);
//        if(minimum.getSymbol().length()>7) { //If minimum ticker is a complete pair NEOUSDT=7 NEOBNBUSDT=10
//            String additionalBuyPair = removeUSDTFromTicker(minimum);
//            Double additionalBuyPairRate = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals(additionalBuyPair)).findFirst().get().getAskPrice();
//            trailingBalance = roundBasedOnPair(additionalBuyPair,Double.parseDouble(trailingBalance)/additionalBuyPairRate);
//            executeSecondBuy(additionalBuyPair,trailingBalance);
//        }
//        if(maximum.getSymbol().length()>7){
//            String additionalSellPair = removeUSDTFromTicker(maximum);
//            Double additionalSellPairRate = bookTickers.stream().filter(ticker -> ticker.getSymbol().equals(additionalSellPair)).findFirst().get().getBidPrice();
//            System.out.println("Additional Sell Pair     : " + additionalSellPair);
//            System.out.println("Additional Sell Pair Rate: " + additionalSellPairRate);
//            System.out.println("Double.parseDouble(trailingBalance) = " + Double.parseDouble(trailingBalance) + " x " + "AdditionalSellPairRate");
//            trailingBalance = roundBasedOnPair(additionalSellPair,Double.parseDouble(trailingBalance));
//            executeAdditionalSell(additionalSellPair,trailingBalance);
//            trailingBalance = roundBasedOnPair(additionalSellPair,Double.parseDouble(trailingBalance)*additionalSellPairRate);
//        }
//        trailingBalance = roundBasedOnPair(lastSellPair,Double.parseDouble(trailingBalance));
//        executeLastSell(lastSellPair,trailingBalance);
//        trailingBalance=roundBasedOnPair(lastSellPair,Double.parseDouble(trailingBalance)*lastSellPairRate);
//
//    }

    private void mockFirstBuy(String firstPair, String quantity) {
        coinAfterExecution = firstPair.toString().replace(initialCoin,"");
        System.out.println("Trade Execution BUY " + firstPair);
        System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
    }
    private void mockSecondBuy(String additionalPairFromMainTicker, String quantity) {
        coinAfterExecution = additionalPairFromMainTicker.replace(coinAfterExecution,"");
        System.out.println("Trade Execution BUY " + additionalPairFromMainTicker);
        System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
    }
    private void mockAdditionalSell(String firstSellPair, String quantity) {
        coinAfterExecution = firstSellPair.replace(coinAfterExecution,"");
        System.out.println("Trade Execution SELL " + firstSellPair);
        System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
    }
    private void mockLastSell(String lastSellPair, String quantity){
        coinAfterExecution = lastSellPair.replace(coinAfterExecution,"");
        System.out.println("Trade Execution SELL " + lastSellPair);
        System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
    }

    private void executeFirstBuy(String firstPair, String quantity) {
        api_trade.buy(getTime(),quantity,firstPair).subscribe(new Consumer<FullResponse>() {
            @Override
            public void accept(FullResponse fullResponse) throws Exception {
                coinAfterExecution = firstPair.toString().replace(initialCoin,"");
                System.out.println("Trade Execution BUY " + fullResponse.getSymbol() + " Quantity: " + quantity + " " + coinAfterExecution);
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
                System.out.println(gson.toJson(fullResponse));
            }
        });
    }

    private void executeSecondBuy(String additionalPairFromMainTicker, String quantity) {
        api_trade.buy(getTime(),quantity,additionalPairFromMainTicker).subscribe(new Consumer<FullResponse>() {
            @Override
            public void accept(FullResponse fullResponse) throws Exception {
                coinAfterExecution = additionalPairFromMainTicker.replace(coinAfterExecution,"");
                System.out.println("Trade Execution BUY " + fullResponse.getSymbol() + " Quantity: " + quantity + " " + coinAfterExecution);
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
                System.out.println(gson.toJson(fullResponse));
            }
        });
    }

    private void executeAdditionalSell(String firstSellPair, String quantity) {
        api_trade.sell(getTime(),quantity, firstSellPair).subscribe(new Consumer<FullResponse>() {
            @Override
            public void accept(FullResponse fullResponse) throws Exception {
                System.out.println("Trade Execution SELL " + fullResponse.getSymbol() + " Quantity: " + quantity + " " + coinAfterExecution);
                coinAfterExecution = firstSellPair.replace(coinAfterExecution,"");
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
                System.out.println(gson.toJson(fullResponse));
            }
        });
    }

    private void executeLastSell(String lastSellPair, String quantity){
        api_trade.sell(getTime(), quantity, lastSellPair).subscribe(new Consumer<FullResponse>() {
            @Override
            public void accept(FullResponse fullResponse) throws Exception {
                System.out.println("Trade Execution SELL " + fullResponse.getSymbol() + " Quantity: " + quantity + " " + coinAfterExecution);
                coinAfterExecution = lastSellPair.replace(coinAfterExecution,"");
                System.out.println("Current Balance: "+ trailingBalance + coinAfterExecution);
                System.out.println(gson.toJson(fullResponse));
            }
        });
    }

    private String roundBasedOnPair(String pair, Double value){
        Symbol symbol = Symbol.valueOf(pair);

        switch(symbol){
            case BTCUSDT: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.000000").format(round(value, 6)));
                return new DecimalFormat("#0.000000").format(round(value, 6));
            case ETHUSDT: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.00000").format(round(value, 5)));
                return new DecimalFormat("#0.00000").format(round(value, 5));
            case BCCUSDT: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.00000").format(round(value, 5)));
                return new DecimalFormat("#0.00000").format(round(value, 5));
            case LTCUSDT: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.00000").format(round(value, 5)));
                return new DecimalFormat("#0.00000").format(round(value, 5));
            case NEOUSDT: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.000").format(round(value,3)));
                return new DecimalFormat("#0.000").format(round(value,3));
            case BNBUSDT: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.00").format(round(value, 2)));
                return new DecimalFormat("#0.00").format(round(value, 2));
            case NEOETH: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.00").format(round(value, 2)));
                return new DecimalFormat("#0.00").format(round(value, 2));
            case NEOBTC: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.00").format(round(value, 2)));
                return new DecimalFormat("#0.00").format(round(value, 2));
            case NEOBNB: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.00").format(round(value, 2)));
                return new DecimalFormat("#0.00").format(round(value, 2));

            //BCC Pairs
            case BCCBNB: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.00000").format(round(value, 5)));
                return new DecimalFormat("#0.000").format(round(value, 5));
            case BCCBTC: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.000").format(round(value, 3)));
                return new DecimalFormat("#0.000").format(round(value, 3));
            case BCCETH: System.out.println("Symbol: "+ symbol + " Rounded:" +new DecimalFormat("#0.000").format(round(value, 3)));
                return new DecimalFormat("#0.000").format(round(value, 3));
            default: return "0.0";
        }
    }

    private void testTradeExecution(Ticker maximum, Ticker minimum) {
        endingBalance = initialBalance;
        String firstBuyPair = extractFirstOrLastPair(minimum);
        String lastSellPair = extractFirstOrLastPair(maximum);
        testExecuteFirstBuy(firstBuyPair);
        if (minimum.getSymbol().length() > 7) { //If minimum ticker is a complepair NEOUSDT=7 NEOBNBUSDT=10
            String additionalPairFromMainTicker = removeUSDTFromTicker(minimum);
            testExecuteSecondBuy(additionalPairFromMainTicker);
        }
        if (maximum.getSymbol().length() > 7) {
            String additionalSellPair = removeUSDTFromTicker(maximum);
            testExecuteAdditionalSell(additionalSellPair);
        }
        testExecuteLastSell(lastSellPair);
    }

    private void testExecuteFirstBuy(String firstPair) {
        api_v3.getSingleTicker(firstPair).subscribe(new Consumer<Ticker>() {
            @Override
            public void accept(Ticker ticker){
                endingBalance = endingBalance/ticker.getPrice();
                coinAfterExecution = firstPair.toString().replace(initialCoin,"");
                System.out.println("Trade Execution BUY " + ticker.getSymbol() + " Price: " + String.format("%.10f",ticker.getPrice()));
                System.out.println("Current Balance: "+ endingBalance + coinAfterExecution);
            }
        });
    }

    private void testExecuteLastSell(String lastSellPair){
        api_v3.getSingleTicker(lastSellPair).subscribe(new Consumer<Ticker>() {
            @Override
            public void accept(Ticker ticker) throws Exception {
                endingBalance = endingBalance*ticker.getPrice();
                coinAfterExecution = lastSellPair.replace(coinAfterExecution,"");
                System.out.println("Trade Execution SELL " + ticker.getSymbol() + " Price: " + String.format("%.10f",ticker.getPrice()));
                System.out.println("Current Balance: "+ endingBalance + coinAfterExecution);
            }
        });
    }

    private void testExecuteAdditionalSell(String firstSellPair) {
        api_v3.getSingleTicker(firstSellPair).subscribe(new Consumer<Ticker>() {
            @Override
            public void accept(Ticker ticker) throws Exception {
                endingBalance = endingBalance * ticker.getPrice();
                coinAfterExecution = firstSellPair.replace(coinAfterExecution, "");
                System.out.println("Trade Execution SELL " + ticker.getSymbol() + " Price: " + String.format("%.10f", ticker.getPrice()));
                System.out.println("Current Balance: " + endingBalance + coinAfterExecution);
            }
        });
    }

    private void testExecuteSecondBuy(String additionalPairFromMainTicker) {
        api_v3.getSingleTicker(additionalPairFromMainTicker).subscribe(new Consumer<Ticker>() {
            @Override
            public void accept(Ticker ticker) {
                coinAfterExecution = additionalPairFromMainTicker.replace(coinAfterExecution,"");
                endingBalance = endingBalance/ticker.getPrice();
                System.out.println("Trade Execution BUY " + ticker.getSymbol() + " Price: " + String.format("%.10f",ticker.getPrice()));
                System.out.println("Current Balance: "+ endingBalance + coinAfterExecution);
            }
        });
    }

    private String removeUSDTFromTicker(Ticker minimumTicker){
        String symbol = minimumTicker.getSymbol();
        symbol = symbol.replace("USDT",""); // BTCLTC is a pair,
        return symbol;
    }

    private String extractFirstOrLastPair(Ticker ticker){
        String symbol = ticker.getSymbol();
        return  symbol.substring(Math.max(0,symbol.length() - 7));
    }

    @Test
    public void assureThatSecondExecutionOfTrimIsWorking(){
        Ticker input1 = new Ticker("NEOETHUSDT",0);
        Ticker input2 = new Ticker("NEOBTCUSDT",0);
        String expected1 = "NEOETH";
        String expected2 = "NEOBTC";
        Assert.assertEquals(expected1, removeUSDTFromTicker(input1));
        Assert.assertEquals(expected2, removeUSDTFromTicker(input2));

    }

    @Test
    public void assureThatMySubstringDelimiterIsAlwaysWorking(){
        Ticker input1 = new Ticker("NEOUSDT",0);
        Ticker input2 = new Ticker("NEOETHUSDT",0);
        String expected1 = "NEOUSDT";
        String expected2 = "ETHUSDT";
        Assert.assertEquals(expected1, extractFirstOrLastPair(input1));
        Assert.assertEquals(expected2, extractFirstOrLastPair(input2));
    }

    @Test
    public void watchBCCPairs(){

        NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        DecimalFormat df = new DecimalFormat("##.##%");

        api_v3.getAllTickers().retry()
                .repeat()
                .subscribe(new Consumer<Ticker[]>() {
                    @Override
                    public void accept(Ticker[] results) throws Exception {

                        List<Ticker> tickers = Arrays.asList(results);
                        List<Ticker> neoList = tickers.stream().filter(ticker -> neoPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                        //System.out.println(gson.toJson(neoList));
                        Ticker BCCUSDT = neoList.stream().filter(ticker -> ticker.getSymbol().equals("BCCUSDT")).findFirst().get();
                        Ticker BCCBTC = neoList.stream().filter(ticker -> ticker.getSymbol().equals("BCCBTC")).findFirst().get();
                        Ticker BCCBNB = neoList.stream().filter(ticker -> ticker.getSymbol().equals("BCCBNB")).findFirst().get();
                        Ticker BCCETH = neoList.stream().filter(ticker -> ticker.getSymbol().equals("BCCETH")).findFirst().get();
                        Ticker BTCUSDT = neoList.stream().filter(ticker -> ticker.getSymbol().equals("BTCUSDT")).findFirst().get();
                        Ticker BNBUSDT = neoList.stream().filter(ticker -> ticker.getSymbol().equals("BNBUSDT")).findFirst().get();
                        Ticker ETHUSDT = neoList.stream().filter(ticker -> ticker.getSymbol().equals("ETHUSDT")).findFirst().get();

                        Double BCCUSD = BCCUSDT.getPrice();
                        Double BCCBTCUSDT = BCCBTC.getPrice()*BTCUSDT.getPrice();
                        Double BCCETHUSDT = BCCETH.getPrice()*ETHUSDT.getPrice();
                        Double BCCBNBUSDT = BCCBNB.getPrice()*BNBUSDT.getPrice();

                        Double NETBCCBTCUSDT = (BCCBTCUSDT-BCCUSD)/BCCUSD;
                        Double NETBCCETHUSDT = (BCCETHUSDT-BCCUSD)/BCCUSD;
                        Double NETBCCBNBUSDT = (BCCBNBUSDT-BCCUSD)/BCCUSD;

                        System.out.println("BCC ->        USDT: "+String.format("%.10f", BCCUSDT.getPrice()));
                        System.out.println("BCC -> BTC -> USDT: "+String.format("%.10f", BCCBTC.getPrice()*BTCUSDT.getPrice()) + " NET: " + String.format("%.10f", BCCBTCUSDT-BCCUSD) + " Percentage: " + df.format(NETBCCBTCUSDT));
                        System.out.println("BCC -> ETH -> USDT: "+String.format("%.10f", BCCETH.getPrice()*ETHUSDT.getPrice()) + " NET: " + String.format("%.10f", BCCETHUSDT-BCCUSD) + " Percentage: " + df.format(NETBCCETHUSDT));
                        System.out.println("BCC -> BNB -> USDT: "+String.format("%.10f", BCCBNB.getPrice()*BNBUSDT.getPrice()) + " NET: " + String.format("%.10f", BCCBNBUSDT-BCCUSD) + " Percentage: " + df.format(NETBCCBNBUSDT));
                    }
                });
    }


    @Test
    public void watchLTCPairs(){
        List<String> neoPairs = new ArrayList<String>() {{add("LTCUSDT");
            add("LTCBTC");
            add("LTCBNB");
            add("LTCETH");
            add("BTCUSDT");
            add("BNBUSDT");
            add("ETHUSDT");}};
        NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        DecimalFormat df = new DecimalFormat("##.##%");
        Gson gson = new Gson();
        api_v3.getAllTickers().retry()
                .repeat()
                .subscribe(new Consumer<Ticker[]>() {
                    @Override
                    public void accept(Ticker[] results) throws Exception {

                        List<Ticker> tickers = Arrays.asList(results);
                        List<Ticker> neoList = tickers.stream().filter(ticker -> neoPairs.contains(ticker.getSymbol())).collect(Collectors.toList());
                        //System.out.println(gson.toJson(neoList));
                        Ticker LTCUSDT = neoList.stream().filter(ticker -> ticker.getSymbol().equals("LTCUSDT")).findFirst().get();
                        Ticker LTCBTC = neoList.stream().filter(ticker -> ticker.getSymbol().equals("LTCBTC")).findFirst().get();
                        Ticker LTCBNB = neoList.stream().filter(ticker -> ticker.getSymbol().equals("LTCBNB")).findFirst().get();
                        Ticker LTCETH = neoList.stream().filter(ticker -> ticker.getSymbol().equals("LTCETH")).findFirst().get();
                        Ticker BTCUSDT = neoList.stream().filter(ticker -> ticker.getSymbol().equals("BTCUSDT")).findFirst().get();
                        Ticker BNBUSDT = neoList.stream().filter(ticker -> ticker.getSymbol().equals("BNBUSDT")).findFirst().get();
                        Ticker ETHUSDT = neoList.stream().filter(ticker -> ticker.getSymbol().equals("ETHUSDT")).findFirst().get();

                        Double LTCUSD = LTCUSDT.getPrice();
                        Double LTCBTCUSDT = LTCBTC.getPrice()*BTCUSDT.getPrice();
                        Double LTCETHUSDT = LTCETH.getPrice()*ETHUSDT.getPrice();
                        Double LTCBNBUSDT = LTCBNB.getPrice()*BNBUSDT.getPrice();

                        Double NETLTCBTCUSDT = (LTCBTCUSDT-LTCUSD)/LTCUSD;
                        Double NETLTCETHUSDT = (LTCETHUSDT-LTCUSD)/LTCUSD;
                        Double NETLTCBNBUSDT = (LTCBNBUSDT-LTCUSD)/LTCUSD;

                        System.out.println("LTC ->        USDT: "+String.format("%.10f", LTCUSDT.getPrice()));
                        System.out.println("LTC -> BTC -> USDT: "+String.format("%.10f", LTCBTC.getPrice()*BTCUSDT.getPrice()) + " NET: " + String.format("%.10f", LTCBTCUSDT-LTCUSD) + " Percentage: " + df.format(NETLTCBTCUSDT));
                        System.out.println("LTC -> ETH -> USDT: "+String.format("%.10f", LTCETH.getPrice()*ETHUSDT.getPrice()) + " NET: " + String.format("%.10f", LTCETHUSDT-LTCUSD) + " Percentage: " + df.format(NETLTCETHUSDT));
                        System.out.println("LTC -> BNB -> USDT: "+String.format("%.10f", LTCBNB.getPrice()*BNBUSDT.getPrice()) + " NET: " + String.format("%.10f", LTCBNBUSDT-LTCUSD) + " Percentage: " + df.format(NETLTCBNBUSDT));
                    }
                });
    }

    @Test
    public void getServerTime(){

        Gson gson = new Gson();
        api_v1.getServerTime()
                .retry()
                .repeat()
                .subscribe(new Consumer<Time>() {
                    @Override
                    public void accept(Time time) throws Exception {
                        System.out.println("Server Time: " + gson.toJson(time.getServerTime()) + " System Time: " + System.currentTimeMillis());
                    }
                });
    }

    @Test
    public void getKLines() {
        long delay = 1000;
        List<CandleStick> candleSticks = new ArrayList<CandleStick>();
        Gson json = new Gson();
        KLines klines = new KLines();
        String pair = "VENBTC";
        Interval interval = Interval.ONE_MINUTE;
        int limit = 60*24*7;
        Gson gson = new Gson();
        api_v1.getKLines(pair,interval.getValue(), limit)
                .subscribe(new Consumer<String[][]>() {
                    @Override
                    public void accept(String[][] strings) throws Exception {
                        //System.out.println(json.toJson(strings));
                        System.out.println(json.toJson(mapToCandleSticks(strings,pair)));
                        computeFiftyDayMovingAverage(mapToCandleSticks(strings,pair));
                    }
                });

        System.out.println("End");
    }

    @Test
    public void testDBConnection(){
        //database = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, DATABASE_NAME).build();

    }

    private void computeFiftyDayMovingAverage(List<CandleStick> candlesticks){
        double average = candlesticks.stream().mapToDouble(candleStick -> candleStick.getClose()).average().getAsDouble();
        for(CandleStick candleStick : candlesticks){
            System.out.println("Close : " + String.format("%.10f", candleStick.getClose()));
        }
        System.out.println(candlesticks.size() + " Day moving Average : "+ String.format("%.10f", average));
    }


    private void computeMovingAverage (List<CandleStick> candlesticks, int units){
        candlesticks.stream();
    }

    //@After
    //public void tearDown() throws InterruptedException{
        //while (true) { Thread.sleep(2000); }
    //}

    private List<CandleStick> mapToCandleSticks(String [][] strings, String pair){
        List<CandleStick> candlesticks = new ArrayList<CandleStick>();
        for (int row = 0; row < strings.length; row++) {
            CandleStick candlestick = new CandleStick();
            candlestick.setPair(pair);
            candlestick.setOpenTime(Long.parseLong(strings[row][0]));
            candlestick.setOpen(Double.parseDouble(strings[row][1]));
            candlestick.setHigh(Double.parseDouble(strings[row][2]));
            candlestick.setLow(Double.parseDouble(strings[row][3]));
            candlestick.setClose(Double.parseDouble(strings[row][4]));
            candlestick.setVolume(Double.parseDouble(strings[row][5]));
            candlestick.setCloseTime(Long.parseLong(strings[row][6]));
            candlestick.setQuoteAssetVolume(Double.parseDouble(strings[row][7]));
            candlestick.setTrades(Integer.parseInt(strings[row][8]));
            candlestick.setTakerBuyBaseAssetVolume(Double.parseDouble(strings[row][9]));
            candlestick.setTakerBuyQuoteAssetVolume(Double.parseDouble(strings[row][10]));
            candlestick.setUnknownData(Double.parseDouble(strings[row][11]));
            candlesticks.add(candlestick);
        }

        return candlesticks;
    }

//    private List<Ticker> getAllAsks(List<BookTicker> bookTickers){
//        List<Ticker> tickers = new ArrayList<>();
//        for(BookTicker bookTicker : bookTickers){
//            Ticker ticker = new Ticker(bookTicker.getSymbol(),bookTicker.getAskPrice());
//        }
//        return tickers;
//    }

//    private List<Ticker> getAllBids(List<BookTicker> bookTickers){
//        List<Ticker> tickers = new ArrayList<>();
//        for(BookTicker bookTicker : bookTickers){
//            Ticker ticker = new Ticker(bookTicker.getSymbol(),bookTicker.getBidPrice());
//        }
//        return tickers;
//    }

    private long getTime(){
        return System.currentTimeMillis()-1000;
    }


    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }








}
